#pragma once
#include "OopsAABB.h"
#include "OopsRaycast.h"
#include "OopsTriangle.h"
#include "OopsAlignmentAllocator.h"
#include "OopsSIMD.h"

namespace Oops
{
  class Collider;
  class PhysicsObj;
  struct Face;

  namespace Broadphase
  {
    class IBroadphase;
  }

  namespace ClothShape
  {
    enum
    {
      Grid,
    };
  }

  struct ClothParams
  {
    ClothParams(float dt, float targetDist, float relaxation, float constraintDamping, float velDamping,
      float frictionScalar, float nodeSize, float breakingDistanceSq, unsigned systemWidth, unsigned systemHeight, int shapeType,
      const Vector3& gravity, const Vector3& handlePos, const Vector3& handleRight,
      const Vector3& handleForward):
      m_dt(dt), m_targetDist(targetDist), m_relaxation(relaxation), m_constraintDamping(constraintDamping),
      m_velDamping(velDamping), m_frictionScalar(frictionScalar), m_nodeSize(nodeSize), m_breakingDistSq(breakingDistanceSq),
      m_systemWidth(systemWidth), m_systemHeight(systemHeight), m_shapeType(shapeType), m_gravity(gravity), m_handlePos(handlePos),
      m_handleRight(handleRight), m_handleForward(handleForward) {}

    ClothParams(void) {}

    float m_dt;
    float m_targetDist;
    float m_relaxation;
    float m_constraintDamping;
    float m_velDamping;
    float m_frictionScalar;
    float m_nodeSize;
    float m_breakingDistSq;
    unsigned m_systemWidth;
    unsigned m_systemHeight;
    int m_shapeType;
    Vector3 m_gravity;
    Vector3 m_handlePos;
    Vector3 m_handleRight;
    Vector3 m_handleForward;
  };

  struct ClothNode
  {
    //Max primitives that can collide with one node at a time
    static const unsigned s_maxPrimsPerNode = 20;

    ClothNode(const Vector3& pos) { SetPos(pos); m_prevPos = m_pos; }
    ClothNode(void) {}

    void Integrate(const ClothParams& params, AABB& aabb, SIMD::SVector4 gravity);
    Vector3 GetVelocity(void);
    Vector3 GetPos(void);
    Vector3 GetPrevPos(void);
    void SetPos(const Vector3& newPos);
    void SetPos(const Vector4& newPos);
    void SetPrevPos(const Vector3& newPos);
    void SetPrevPos(const Vector4& newPos);

    SIMD::SVector4 m_pos;
    SIMD::SVector4 m_prevPos;
    //Flag where the index of the bits indicates the indexes of primitives in ClothSystem::m_collidingObjects
    //that this node are too far away to consider collisions with
    std::bitset<s_maxPrimsPerNode> m_collisionFlag;
  };

  struct ClothNodeConstraint
  {
    ClothNodeConstraint(ClothNode* nodeA, ClothNode* nodeB):
      m_nodeA(nodeA), m_nodeB(nodeB), m_broken(false) {}
    ClothNodeConstraint(void):
      m_nodeA(nullptr), m_nodeB(nullptr), m_broken(false) {}
    ~ClothNodeConstraint(void) {}

    float Solve(const ClothParams& params);
    void Draw(void);

    ClothNode* m_nodeA;
    ClothNode* m_nodeB;
    bool m_broken;
  };

  struct ClothPinConstraint
  {
    ClothPinConstraint(ClothNode* node, PhysicsObj* pin, const Vector3& anchor):
      m_node(node), m_pin(pin), m_anchor(anchor) {}
    ClothPinConstraint(void): m_node(nullptr), m_pin(nullptr) {}

    void Solve(void);
    void Draw(void);

    ClothNode* m_node;
    PhysicsObj* m_pin;
    Vector3 m_anchor;
  };

  namespace NodeTriFlags
  {
    enum
    {
      Skip,
      //Means it is removed at the beginning of next frame no matter what.
      //Used for environment triangles, which aren't expensive to cache
      Temporary,
      Count
    };
  }

  struct NodeTri
  {
    NodeTri(void): m_tri(nullptr), m_collider(nullptr) {}
    NodeTri(Face* tri, Collider* collider, bool skip):
      m_tri(tri), m_collider(collider) { SetSkip(skip); }

    bool GetSkip(void) { return m_flags[NodeTriFlags::Skip]; }
    bool GetTemporary(void) { return m_flags[NodeTriFlags::Temporary]; }
    void SetSkip(bool value) { m_flags[NodeTriFlags::Skip] = value; }
    void SetTemporary(bool value) { m_flags[NodeTriFlags::Temporary] = value; }

    Face* m_tri;
    Collider* m_collider;
    std::bitset<NodeTriFlags::Count> m_flags;
  };

  //Cached triangles close to a node for collision detection
  struct NodeTriangles
  {
    static const unsigned s_maxTrisPerNode = 100;

    NodeTriangles(void): m_size(0) {}

    //Add triangle to m_tris, discarding duplicates and not inserting above capacity
    NodeTri* AddTri(Face* tri, Collider* collider, bool skip);
    //Swap with back to remove from index. Caller is responsible for valid range
    void RemoveIndex(unsigned index);
    void RemoveTri(Face* tri);
    void RemoveCollider(Collider* collider);
    void Clear(void);

    //A triangle for each convex object in the aabb of the cloth, and all environment triangles in it
    NodeTri m_tris[s_maxTrisPerNode];
    //This will never be bigger than maxCollisionsPerNode, so it could be a char, but because of alignment I don't think that helps anything
    unsigned m_size;
  };

  class ClothSystem
  {
  public:
    DECLARE_INTRUSIVE_NODE(ClothSystem);

    //Percentage aabb is padded
    static float s_aabbPercentPad;
    //Amount added to minimum axis of aabb
    static float s_aabbMinAxisPad;
    //Distance away from closest point an object has to be before it ignores collision with that object
    static float s_triIgnoreDistSq;
    //Roughly an extra percentage tolarance for considering a point to be inside a triangle face.
    //used to discourage extra triangle traversals
    static float s_triAdjPadding;

    void Initialize(void);
    void CreateNodes(void);

    void Integrate(float dt);
    void StoreCollidingObjects(Broadphase::IBroadphase* broadphase);

    void SolveConstraints(float dt);
    void SolveCollisions(float dt, int iteration);

    void SetParams(const ClothParams& params) { m_params = params; }
    void SetOwner(Collider* collider) { m_owner = collider; }

    void AddPin(PhysicsObj* pin, ClothNode* node, const Vector3& anchor);
    void RemovePin(PhysicsObj* pin);

    void Draw(void);
  private:
    void CreateGrid(void);

    //Iteration over objects and early rejections before calling Resolves
    void SolvePrimitiveCollisions(float dt, int iteration, float nodeSizeSq, float frictionScalar);
    void SolveTriangleCollisions(float dt, int iteration, float nodeSizeSq, float frictionScalar);
    void SolveTriangleCollision(Face* face, Face* oldFace, CollisionDetection::PointBarycentric& pBary,
      int iteration, float nodeSizeSq, ClothNode& node, NodeTri& nodeTri);
    //Detect and resolve node collision given closest point on object to node
    void ResolveClosestPoint(ClothNode& node, SIMD::SVector4 closestPoint, SIMD::SVector4 normal, float normalLengthSq, 
      Collider* curCollider, float nodeSizeSq, const SIMD::SVector4* normalDir);
    void ResolveCollision(ClothNode* node, SIMD::SVector4 surfaceHit,SIMD::SVector4 normal, const ClothParams& params);

    //Assign close triangles to each node, then do collision detection on them while the data is there
    void CacheConvexTriangles(Collider* collider, float nodeSizeSq, float frictionScalar);
    void ClearConvexTriangles(void);
    void CacheEnvironmentTriangles(float nodeSizeSq, float frictionScalar);

    //Gets cached calculations to re-use among iterations and other triangles
    const CollisionDetection::TriBarycentric& GetTriBarycentric(Face* face);
    Face* GetFaceContainingPoint(SIMD::SVector4 point, Face* currentFace, CollisionDetection::PointBarycentric& pBary);

    Collider* m_owner;
    ClothParams m_params;
    std::vector<ClothNode, AlignmentAllocator<ClothNode, SIMD_ALIGNMENT>> m_nodes;
    //These are seperate containers since the constraints have different lengths,
    //but are all the same within the container, so it would be a waste to store it per constraint
    std::vector<ClothNodeConstraint> m_orthoConstraints;
    std::vector<ClothNodeConstraint> m_diagConstraints;
    std::vector<ClothPinConstraint> m_pinConstraints;

    std::vector<Collider*> m_collidingObjects;
    //Temporary container used to determine new and removed objects when getting new ones
    std::vector<Collider*> m_newCollidingObjects;
    //All colliders that changed between this frame and last, true means add it, false means it should be removed
    std::vector<std::pair<Collider*, bool>> m_colliderDiff;

    std::vector<Face*> m_collidingFaces;
    //First is index after last face belonging to that collider, second is collider
    std::vector<std::pair<size_t, Collider*>> m_faceColliders;
    //Triangles close to each node. Used along with adjacency info to speed up collisions
    std::vector<NodeTriangles> m_nodeTris;
    //If there are any node collisions this frame. Used to avoid having to repeatedly clear container if it is never populated
    bool m_areNodeTris;
    AABB m_aabb;
    SIMD_ALIGN CollisionDetection::ClosestPointer m_closestPointer;
  };
}