#include "OopsClothSystem.h"
#include "OopsBroadphase.h"
#include "OopsCollider.h"
#include "OopsObject.h"

#include <xmmintrin.h>
#include <DirectXMath.h>


namespace Oops
{
  float ClothSystem::s_aabbPercentPad = 0.1f;
  float ClothSystem::s_aabbMinAxisPad = 0.5f;
  float ClothSystem::s_triIgnoreDistSq = 3.0f;

  void ClothNode::Integrate(const ClothParams& params, AABB& aabb, SIMD::SVector4 gravity)
  {
    SIMD::SVector4 velocity = m_pos - m_prevPos;
    m_prevPos = m_pos;
    m_pos += velocity*params.m_velDamping + gravity;

    aabb.Add(GetPos());
  }

  void ClothNode::SetPos(const Vector3& newPos)
  {
    SetPos(Vector4(newPos, 0.0f));
  }

  void ClothNode::SetPos(const Vector4& newPos)
  {
    SIMD_ALIGN Vector4 aNewPos(newPos);
    m_pos = SIMD::Load(aNewPos);
  }

  void ClothNode::SetPrevPos(const Vector3& newPos)
  {
    SetPrevPos(Vector4(newPos, 0.0f));
  }

  void ClothNode::SetPrevPos(const Vector4& newPos)
  {
    SIMD_ALIGN Vector4 aNewPos(newPos);
    m_prevPos = SIMD::Load(aNewPos);
  }

  Vector3 ClothNode::GetVelocity(void)
  {
    SIMD_ALIGN Vector4 result;
    SIMD::SVector4 vel = m_pos - m_prevPos;
    SIMD::Store(result, vel);
    return Vector3(result);
  }

  Vector3 ClothNode::GetPos(void)
  {
    return SIMD::GetVector3(m_pos);
  }

  Vector3 ClothNode::GetPrevPos(void)
  {
    return SIMD::GetVector3(m_prevPos);
  }

  float ClothNodeConstraint::Solve(const ClothParams& params)
  {
    if(m_broken)
      return 0.0f;

    //Calculate position error
    SIMD::SVector4 posCorrection = m_nodeA->m_pos - m_nodeB->m_pos;

    //This is black magic to avoid having to take sqrt from:
    //http://www.gamasutra.com/view/feature/131313/advanced_character_physics.php
    //Mathematically, what we do is approximate the square root function by its 1st order
    //Taylor-expansion at a neighborhood of the rest length r
    //(this is equivalent to one Newton-Raphson iteration with initial guess r).
    float dist2 = SIMD::Length2(posCorrection);
    float posError = params.m_targetDist/(dist2 + params.m_targetDist) - 0.5f;
    //Will be zero length if constraints are satisfied
    posCorrection *= posError*params.m_relaxation;

    //Damping is commented out because it makes this way more expensive
    //I'll put it back if it creates effects we want
    //Calculate damping
    /*Vector3 relVel = m_nodeA->GetVelocity() - m_nodeB->GetVelocity();
    float dampScalar = glm::dot(bToA, relVel)*params.m_constraintDamping;
    Vector3 damping = relVel*dampScalar;*/

    //Vector3 impulse = posCorrection;// + damping;
    m_nodeA->m_pos += posCorrection;
    m_nodeB->m_pos -= posCorrection;
    if(dist2 > params.m_breakingDistSq)
      m_broken = true;
    return posError;
  }

  void ClothNodeConstraint::Draw(void)
  {
    if(!m_broken)
      DrawLine(SIMD::GetVector3(m_nodeA->m_pos), SIMD::GetVector3(m_nodeB->m_pos));
  }

  void ClothPinConstraint::Solve(void)
  {
    //Right now this is solved as a hard pin. If this looks too rigid I could do a soft solve within a radius
    //and hard after that
    Vector3 worldAnchor = m_pin->ToWorld(m_anchor);
    m_node->SetPos(worldAnchor);
  }

  void ClothPinConstraint::Draw(void)
  {
    Vector3 worldAnchor = m_pin->ToWorld(m_anchor);
    DrawBox(worldAnchor, 0.1f);
  }

  NodeTri* NodeTriangles::AddTri(Face* tri, Collider* collider, bool skip)
  {
    //If this is false, the node has reached max collisions.
    //if this happens often enough for it to be visible, max
    //should be increased
    if(m_size < s_maxTrisPerNode)
      m_tris[m_size++] = NodeTri(tri, collider, skip);
    return &m_tris[m_size - 1];
  }

  void NodeTriangles::RemoveIndex(unsigned index)
  {
    //Don't need to worry about --0 because I don't use
    //this function in a way that will cause that
    --m_size;
    if(m_size)
      m_tris[index] = m_tris[m_size];
  }

  void NodeTriangles::RemoveTri(Face* tri)
  {
    for(unsigned i = 0; i < m_size; ++i)
      if(m_tris[i].m_tri == tri)
        return RemoveIndex(i);
  }

  void NodeTriangles::RemoveCollider(Collider* collider)
  {
    for(unsigned i = 0; i < m_size; ++i)
      if(m_tris[i].m_collider == collider)
        return RemoveIndex(i);
  }

  void NodeTriangles::Clear(void)
  {
    m_size = 0;
  }

  //Defaults that don't look stupid
  void ClothSystem::Initialize(void)
  {
    m_params.m_systemWidth = 20;
    m_params.m_systemHeight = 20;
    m_params.m_constraintDamping = 0.001f;
    m_params.m_velDamping = 0.999f;
    m_params.m_frictionScalar = 1.0f;
    m_params.m_gravity = -Axes::Y*50.0f;
    m_params.m_nodeSize = 0.3f;
    m_params.m_relaxation = 1.0f;
    m_params.m_shapeType = ClothShape::Grid;
    m_params.m_targetDist = 0.5f;
    m_params.m_handleRight = Axes::X;
    m_params.m_handleForward = Axes::Z;
  }

  void ClothSystem::CreateNodes(void)
  {
    unsigned width = m_params.m_systemWidth;
    unsigned height = m_params.m_systemHeight;
    m_nodes.clear();
    m_orthoConstraints.clear();
    m_diagConstraints.clear();
    m_nodes.resize(width*height);
    //*4 because max constraints on one node is 4, edges won't have this, so it's a little less
    m_orthoConstraints.reserve(width*height*2);
    m_diagConstraints.reserve(width*height*2);

    for(unsigned i = 0; i < height; ++i)
      for(unsigned j = 0; j < width; ++j)
      {
        unsigned curIndex = i*width + j;
        //Position node
        m_nodes[curIndex] = ClothNode(m_params.m_handlePos + 
          m_params.m_handleRight*static_cast<float>(j)*m_params.m_targetDist +
          m_params.m_handleForward*static_cast<float>(i)*m_params.m_targetDist);

        //Create constraints
        ClothNode* curNode = &m_nodes[curIndex];
        bool topRow = i + 1 >= height;
        bool lastCol = j + 1 >= width;
        bool firstCol = j == 0;
        //Left neighbor
        if(!firstCol)
          m_orthoConstraints.push_back(ClothNodeConstraint(curNode, &m_nodes[curIndex - 1]));

        if(!topRow)
        {
          //Upper neighbor
          m_orthoConstraints.push_back(ClothNodeConstraint(curNode, &m_nodes[curIndex + width]));
          //Upper left neighbor
          if(!firstCol)
            m_diagConstraints.push_back(ClothNodeConstraint(curNode, &m_nodes[curIndex + width - 1]));
          //Upper right neighbor
          if(!lastCol)
            m_diagConstraints.push_back(ClothNodeConstraint(curNode, &m_nodes[curIndex + width + 1]));
        }
      }

    m_nodeTris.resize(height*width);
  }

  void ClothSystem::Integrate(float dt)
  {
    //No need to do this multiplication for each node, so do it here
    //Acceleration in verlet is multiplied by dt^2
    SIMD_ALIGN Vector4 tempGrav = Vector4(m_params.m_gravity*(dt*dt), 0.0f);
    SIMD::SVector4 sGrav = SIMD::Load(tempGrav);
    m_params.m_dt = dt;

    //Reset aabb. Without this it would always contain the aabb it started as
    if(!m_nodes.empty())
      m_aabb.m_min = m_aabb.m_max = m_nodes.front().GetPos();

    for(auto& curNode : m_nodes)
      curNode.Integrate(m_params, m_aabb, sGrav);
    //Pad a bit to make sure no collisions are missed.
    //This magic number should be stored somewhere
    m_aabb.Pad(s_aabbPercentPad);
    //Since pad is a percentage, and won't do anything if an element is zero, pad along least significant axis manually as well
    int minAxis = LeastSignificantAxis(m_aabb.m_max - m_aabb.m_min);
    m_aabb.m_max[minAxis] += s_aabbMinAxisPad;
    m_aabb.m_min[minAxis] -= s_aabbMinAxisPad;
  }

  void ClothSystem::StoreCollidingObjects(Broadphase::IBroadphase* broadphase)
  {
    if(m_owner->IsTrigger())
    {
      m_newCollidingObjects.clear();
      m_collidingFaces.clear();
      m_faceColliders.clear();
      return;
    }

    GetCollidingParams params(&m_newCollidingObjects, &m_collidingFaces, &m_faceColliders, &m_aabb, m_owner->m_group, true, true);
    broadphase->GetColliding(params);
    m_areNodeTris = !m_collidingFaces.empty();
    m_colliderDiff.clear();

    //Determine objects that are no longer within cloth range
    for(unsigned i = 0; i < m_collidingObjects.size();)
    {
      Collider* oldObj = m_collidingObjects[i];
      //We don't care about keeping track of primitives, this is just for caching triangles
      if(oldObj->m_model->m_shapeType == ShapeType::Generic)
        m_areNodeTris = true;

      bool found = false;
      for(unsigned j = 0; j < m_newCollidingObjects.size(); ++j)
        if(m_newCollidingObjects[j] == oldObj)
        {
          found = true;
          //Remove from new so at the end all that is left is objects that weren't in m_collidingObjects
          SwapRemove(m_newCollidingObjects, j);
          break;
        }

      if(!found)
      {
        //Add to list of changes, indicating that it should be removed from node caches
        m_colliderDiff.push_back({ oldObj, false });
        //Remove, so only objects common to old and new are in old
        SwapRemove(m_collidingObjects, i);
      }
      else
        ++i;
    }

    //All objects left in new container are new this frame
    for(auto newObj : m_newCollidingObjects)
    {
      //Add to list of changes, indicating it should be added to node caches
      if(newObj->m_model->m_shapeType == ShapeType::Generic)
      {
        continue;
        m_colliderDiff.push_back({ newObj, true });
        m_areNodeTris = true;
      }
      //Merge new container with old, because old will be used for iteration
      m_collidingObjects.push_back(newObj);
    }
  }

  void ClothSystem::CacheConvexTriangles(Collider* collider, float nodeSizeSq, float /*frictionScalar*/)
  {
    //Everything colliding with cloth should, because without adjacency info collisions are far too slow
    if(!collider->m_model->m_useHalfEdges)
      return;

    //This is the expensive traversal that finds the initial, all others should be often 0-1 triangles away
    Face* curTri = collider->GetFaceContainingPoint(m_nodes[0].m_pos);
    //Rare edge case that would happen due to some adjacency structure error
    if(!curTri)
      return Log("Cloth node couldn't find triangle on model");
    m_nodeTris[0].AddTri(curTri, collider, false);

    //Start first column at 1 since it was added already above
    //Loop in a snake pattern so all nodes are as close to each-other as possible,
    //meaning less triangle traversals
    unsigned index = 1;
    bool increasing = true;

    for(unsigned row = 0; row < m_params.m_systemHeight; ++row)
    {
      for(unsigned col = 0; col < m_params.m_systemWidth; ++col)
      {
        //Skip first because it was already inserted above
        if(!row && !col)
          continue;

        Face* oldTri = curTri;
        CollisionDetection::PointBarycentric pBary;
        curTri = GetFaceContainingPoint(m_nodes[index].m_pos, curTri, pBary);
        //Rare edge case that would happen due to some adjacency structure error
        if(!curTri)
          return Log("Cloth node couldn't find triangle on model");
        NodeTri* nodeTri = m_nodeTris[index].AddTri(curTri, collider, false);

        //It shouldn't be possible to be null, but just in case
        if(nodeTri)
          SolveTriangleCollision(curTri, oldTri, pBary, 0, nodeSizeSq, m_nodes[index], *nodeTri);

        //Iterate in a zig-zag pattern so each query is close to the one before to take advantage of hill climbing
        index += increasing ? 1 : -1;
      }

      //The last increment always overshoots, so correct it
      index += increasing ? -1 : 1;
      increasing = !increasing;
      index += m_params.m_systemWidth;
    }
  }

  void ClothSystem::CacheEnvironmentTriangles(float nodeSizeSq, float /*frictionScalar*/)
  {
    return;
    if(m_collidingFaces.empty())
      return;

    for(size_t i = 0; i < m_params.m_systemWidth*m_params.m_systemHeight; ++i)
    {
      NodeTriangles& tris = m_nodeTris[i];
      size_t faceColliderIndex = 0;
      for(size_t j = 0; j < m_collidingFaces.size(); ++j)
      {
        Face* face = m_collidingFaces[j];

        if(j >= m_faceColliders[faceColliderIndex].first)
          ++faceColliderIndex;
        Collider* collider = m_faceColliders[faceColliderIndex].second;

        CollisionDetection::PointBarycentric pBary;
        NodeTri* nodeTri = tris.AddTri(face, collider, false);
        if(nodeTri)
        {
          nodeTri->SetTemporary(true);
          SolveTriangleCollision(face, nullptr, pBary, 0, nodeSizeSq, m_nodes[i], *nodeTri);
        }
      }
    }
  }

  void ClothSystem::SolveConstraints(float dt)
  {
    m_params.m_dt = dt;
    float orthoDist = m_params.m_targetDist;
    //sqrt(2), since it is supposed to be the hypotenuse of a triangle with two sides of targetDist length
    float diagDist = m_params.m_targetDist * 1.414f;

    for(auto& curConstraint : m_pinConstraints)
      curConstraint.Solve();

    m_params.m_targetDist = orthoDist*orthoDist;
    for(auto& curConstraint : m_orthoConstraints)
      curConstraint.Solve(m_params);

    m_params.m_targetDist = diagDist*diagDist;
    for(auto& curConstraint : m_diagConstraints)
      curConstraint.Solve(m_params);

    m_params.m_targetDist = orthoDist;
  }

  void ClothSystem::SolveCollisions(float dt, int iteration)
  {
    float nodeSizeSq = m_params.m_nodeSize*m_params.m_nodeSize;
    float frictionScalar = m_params.m_frictionScalar;

    SolvePrimitiveCollisions(dt, iteration, nodeSizeSq, frictionScalar);
    SolveTriangleCollisions(dt, iteration, nodeSizeSq, frictionScalar);

    //Do this after first collision solve so if they are to be evaluated in upcoming frames
    //they don't get evaluated one more time than if they went before first solve
    if(!iteration)
    {
      for(auto newObj : m_colliderDiff)
        //If this is to be inserted. All falses should have been removed in first triangle solve, but just to make sure
        if(newObj.second && newObj.first->m_model->m_shapeType == ShapeType::Generic)
          CacheConvexTriangles(newObj.first, nodeSizeSq, frictionScalar);

      CacheEnvironmentTriangles(nodeSizeSq, frictionScalar);
    }

    //Restore original value, as this was used as a temporary for node input
    m_params.m_frictionScalar = frictionScalar;
  }

  void ClothSystem::SolvePrimitiveCollisions(float /*dt*/, int iteration, float nodeSizeSq, float frictionScalar)
  {
    //Solve non-triangle meshes
    //Silly bools to reset collisionFlags for each node once on the first iteration of the first object,
    //which because of skipping may not be objects[0]
    bool flagsReset = false;
    bool resetting = true;
    for(size_t i = 0; i < m_collidingObjects.size(); ++i)
    {
      Collider* curCollider = m_collidingObjects[i];
      //These are handled by triangle collisions
      if(curCollider->m_model->m_shapeType == ShapeType::Generic)
      {
        //Updates the triangles if they aren't already
        if(!iteration)
          curCollider->GetWorldModel();
        continue;
      }

      //Precompute these so they can be re-used per node.
      //I could also calculate them once per frame, but that's overkill
      SIMD_ALIGN CollisionDetection::ClosestParams closestParams(*curCollider->m_owner);

      //Compute multiplied value so it doesn't need to be done per node
      if(curCollider->m_material)
        m_params.m_frictionScalar = curCollider->m_material->m_friction*frictionScalar;
      //I loop over all nodes per object instead of all objects per node because I know
      //nodes are contiguous in memory, and objects probably aren't, so cache coherence should
      //be better this way
      for(auto& node : m_nodes)
      {
        //Clear flags the first time through the nodes this frame
        if(!iteration && resetting)
        {
          node.m_collisionFlag.reset();
          flagsReset = true;
        }

        //If this object was deemed too far away from this node in a previous iteration, skip it
        if(node.m_collisionFlag[i])
          continue;

        closestParams.m_sPoint = node.m_pos;
        SIMD::SVector4 contactPoint = m_closestPointer.GetClosestOnCollider(curCollider, closestParams);
        SIMD::SVector4 normal = node.m_pos - contactPoint;
        float normalLengthSq = SIMD::Length2(normal);

        //Only compare on first frame. I don't know how expensive a float comparison is,
        //so it may or may not be cheaper to just always compare and omit !iteration
        if(!iteration && normalLengthSq > s_triIgnoreDistSq)
        {
          //Mark for ignoring in future iterations
          node.m_collisionFlag.set(i);
          continue;
        }

        ResolveClosestPoint(node, contactPoint, normal, normalLengthSq, curCollider, nodeSizeSq, nullptr);
      }

      //A little awkward, but we only want to reset flags on the first iteration, but for all nodes
      if(flagsReset)
        resetting = false;
    }
  }

  void ClothSystem::SolveTriangleCollisions(float /*dt*/, int iteration, float nodeSizeSq, float frictionScalar)
  {
    if(!m_areNodeTris)
      return;

    //I should be doing this per object to the right value, but I'll figure that out later
    m_params.m_frictionScalar = frictionScalar;

    for(unsigned i = 0; i < m_params.m_systemWidth*m_params.m_systemHeight; ++i)
    {
      NodeTriangles& tris = m_nodeTris[i];
      ClothNode& node = m_nodes[i];
      //Don't iterate j because elements may be removed because of their distance
      for(unsigned j = 0; j < tris.m_size;)
      {
        NodeTri& nodeTri = tris.m_tris[j];
        //On the first frame they get flagged for skipping, so if this isn't that frame, skip if flagged
        if(nodeTri.GetSkip() && iteration)
        {
          ++j;
          continue;
        }

        //Make sure this tri hasn't been flagged for removal
        bool deleted = false;
        for(unsigned k = 0; k < m_colliderDiff.size(); ++k)
          if(!m_colliderDiff[k].second && m_colliderDiff[k].first == nodeTri.m_collider)
          {
            tris.RemoveIndex(j);
            SwapRemove(m_colliderDiff, k);
            deleted = true;
            break;
          }
        if(deleted)
          continue;

        Face*& face = nodeTri.m_tri;
        Face* oldFace = nodeTri.m_tri;

        //Store calculations from GetFace... to be used in getclosest if it's the same triangle
        CollisionDetection::PointBarycentric pBary;
        //This assumes a node will never move more than 1 triangle away,
        //but the speed gained by this is really important
        //!iteration assumes that it won't move to another triangle in the course of 1 frame, if this causes instability, remove the check
        if(!iteration)
        {
          //Clear temporaries on first frame
          if(nodeTri.GetTemporary())
          {
            tris.RemoveIndex(j);
            continue;
          }

          face = face->GetFaceTowardsPoint(node.m_pos, &pBary);
        }
        else
          oldFace = nullptr;

        float temp = m_params.m_frictionScalar;
        if(nodeTri.m_collider)
          m_params.m_frictionScalar *= nodeTri.m_collider->m_material->m_friction;
        SolveTriangleCollision(face, oldFace, pBary, iteration, nodeSizeSq, node, nodeTri);
        m_params.m_frictionScalar = temp;
        ++j;
      }
    }
  }

  void ClothSystem::SolveTriangleCollision(Face* face, Face* oldFace, CollisionDetection::PointBarycentric& pBary,
    int iteration, float nodeSizeSq, ClothNode& node, NodeTri& nodeTri)
  {
    //Rare edge case that would happen due to some adjacency structure error
    if(!face)
      return Log("Cloth node couldn't find triangle on model");

    const auto& tBary = GetTriBarycentric(face);
    //If this is a new face, get new barycentric info for it
    if(face != oldFace)
      pBary.SetPointInfo(node.m_pos, tBary);

    SIMD::SVector4 closestPoint;
    if(pBary.PointInsideTri(tBary))
    {
      //If the point is already inside, we don't need to use GetClosestToPoint which does extra math
      //to determine how to clamp. So solve barycentric equation as is, using cached values
      pBary.m_u *= tBary.m_invDet;
      pBary.m_v *= tBary.m_invDet;
      closestPoint = pBary.BarycentricToCartesian(tBary);
    }
    else
      closestPoint = face->GetClosestToPoint(tBary, pBary);

    SIMD::SVector4 normal = node.m_pos - closestPoint;
    float normalLengthSq = SIMD::Length2(normal);

    //Flag this triangle for skipping later if it is too far to be considered
    if(iteration == 0 && normalLengthSq > s_triIgnoreDistSq)
      nodeTri.SetSkip(true);
    else
    {
      nodeTri.SetSkip(false);
      ResolveClosestPoint(node, closestPoint, normal, normalLengthSq, nullptr, nodeSizeSq, &tBary.m_normal);
    }
  }

  void ClothSystem::ResolveClosestPoint(ClothNode& node, SIMD::SVector4 closestPoint, SIMD::SVector4 normal, float normalLengthSq, 
    Collider* curCollider, float nodeSizeSq, const SIMD::SVector4* normalDir)
  {
    //No Collision
    if(normalLengthSq > nodeSizeSq)
      return;

    if(normalDir && SIMD::DotFloat(normal, *normalDir) < 0.0f)
        normal = -normal;

    //If the distance is nonzero, the center of the cloth node is not in the object, so
    //center to closest point is acceptable normal. So just normalize it
    if(normalLengthSq > EPSILON)
      normal *= 1.0f/sqrt(normalLengthSq);
    //Otherwise the cloth node center is inside the collider. Approximate contact point as
    //nodeSize along the vector between them, as this is the minimum it could be penetrating.
    //That will push it out by that much this iteration,
    //and hopefully eventually all the way out, given enough iterations
    else
    {
      //If this is zero too, the cloth node is at the collider's center of mass, which is so
      //messed up I won't deal with it. The constraints will move it soon and then it won't be
      SIMD_ALIGN Vector4 tempNormal;
      if(curCollider)
        tempNormal = Vector4(SafeNormalize(node.GetPos() - curCollider->GetWorldCenterOfMass()), 0.0f);
      else
        tempNormal = Vector4(Axes::Y, 0.0f);
      closestPoint = node.m_pos;
      normal = SIMD::Load(tempNormal);
    }
    ResolveCollision(&node, closestPoint, normal, m_params);
  }

  void ClothSystem::ResolveCollision(ClothNode* node, SIMD::SVector4 surfaceHit, SIMD::SVector4 normal, const ClothParams& params)
  {
    //Resolve collision by moving it out of contact with object
    node->m_pos = surfaceHit + normal*params.m_nodeSize;

    //Approximate friction by modifying particle's effective velocity
    //This could be scaled by contact depth to simulate magnitude of normal force, but that seems like overkill
    SIMD::SVector4 vel = node->m_pos - node->m_prevPos;
    SIMD::SVector4 frictionImpulse = vel - normal*SIMD::DotVec(vel, normal);
    node->m_prevPos += frictionImpulse*params.m_frictionScalar;
  }

  void ClothSystem::Draw(void)
  {
    for(auto& curConstraint : m_orthoConstraints)
      curConstraint.Draw();
    //for(auto& curConstraint : m_diagConstraints)
      //curConstraint.Draw();
    for(auto& curConstraint : m_pinConstraints)
      curConstraint.Draw();

    //Draw objects cloth thinks are within its aabb
    /*for(auto oldObj : m_collidingObjects)
      DrawLine(Vector3(20.0f), oldObj->m_owner->GetWorldPos());*/
  }

  void ClothSystem::CreateGrid(void)
  {
    switch(m_params.m_shapeType)
    {
    case ClothShape::Grid: CreateNodes(); break;
    }
  }

  void ClothSystem::AddPin(PhysicsObj* pin, ClothNode* node, const Vector3& anchor)
  {
    m_pinConstraints.push_back(ClothPinConstraint(node, pin, anchor));
  }

  void ClothSystem::RemovePin(PhysicsObj* pin)
  {
    for(size_t i = 0; i < m_pinConstraints.size();)
      if(m_pinConstraints[i].m_pin == pin)
        SwapRemove(m_pinConstraints, i);
      else
        ++i;
  }

  const CollisionDetection::TriBarycentric& ClothSystem::GetTriBarycentric(Face* face)
  {
    return face->GetBarycentric();
  }

  //This looks really ugly, but performs MUCH faster than using a set to avoid duplicates,
  //so it's worth it the idea is traversing adjecent triangles to currentFace while detecting duplicates
  //using a linear search through a static array. When the array gets full I wrap around, placing new
  //elements at 0, 1, ... again and checking the whole container every time for duplicates.
  Face* ClothSystem::GetFaceContainingPoint(SIMD::SVector4 point, Face* currentFace, CollisionDetection::PointBarycentric& pBary)
  {
    //Assumes that we will never traverse a loop bigger than 10, meaning that
    //it never takes 11 traversals to get back to a triangle I already traversed
    const unsigned max = 10;
    //Null terminated list of triangles that have already been traversed to avoid traversing forever.
    Face* traversedFaces[max] = {currentFace, nullptr};
    //If the container has been filled and is wrapping around
    bool wrapAround = false;

    //While we are still traversing new triangles
    //If a triangle containing the point is found, it will return itself, triggering this too
    //This will in most cases just confirm that the point is still in the triangle
    int iterations = 0;
    while(iterations++ < 200)
    {
      Face* oldFace = currentFace;
      currentFace = currentFace->GetFaceTowardsPoint(point, &pBary);
      //If they match, can return with good point info
      if(oldFace == currentFace)
        return currentFace;

      //Insert this into traversed list, checking for duplicates to avoid going in circles
      bool inserted = false;
      //Used to store insertion position when whole container needs to be checked due to wrap around
      int insertIndex = 0;

      for(int i = 0; i < max; ++i)
      {
        Face* traversedFace = traversedFaces[i];
        if(traversedFace == currentFace)
        {
          //Need to update pBary since this is some old triangle we don't have pBary for
          pBary.SetPointInfo(point, GetTriBarycentric(currentFace));
          return currentFace;
        }

        //End reached, insert here
        if(!traversedFace)
        {
          if(!wrapAround)
          {
            traversedFaces[i] = currentFace;
            inserted = true;
            //Break out back into triangle traversal loop
            break;
          }
          else
            insertIndex = i;
        }
      }

      //Container is full or we're wrapping around
      if(!inserted)
      {
        //Start another loop around
        wrapAround = true;
        traversedFaces[insertIndex] = currentFace;
        traversedFaces[insertIndex + 1 >= max ? 0 : insertIndex + 1] = nullptr;
      }
    }

    if(iterations == 200)
      Log("Infinite loop broken in GetFaceContainingPoint in ClothSystem");

    //Shouldn't happen, unless an infinite loops was encountered, which- shouldn't happen
    //Need to update pBary since this is some old triangle we don't have pBary for
    pBary.SetPointInfo(point, GetTriBarycentric(currentFace));
    return currentFace;
  }
}