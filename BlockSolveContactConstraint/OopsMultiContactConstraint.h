#pragma once
#include "OopsContactConstraint.h"
#include "OopsFrictionConstraint.h"
#include "OopsMultiConstraint.h"

namespace Oops
{
  struct Face;

  namespace Constraints
  {
    class MultiContactConstraint : public MultiConstraint
    {
    public:
      DECLARE_INTRUSIVE_NODE(MultiContactConstraint)

      MultiContactConstraint(Rigidbody* objA, Rigidbody* objB);
      MultiContactConstraint(void);
      void DerivedInitialize(void);
      void GetMassMatrix(float**& in, unsigned setID);
      void CalculateMass(Constraint** constraints, unsigned setID);
      void Solve(unsigned iteration);
      void SolvePosition(bool setupJacobian, unsigned iteration);
      void FirstIteration(void);
      //Force limits on contact constraints don't make sense
      void SetForceLimit(float) {};
      bool EnforceContact(ContactPoint& contact);
      void AddContact(Manifold& newManifold);
      void DrawContactInfo(void);
      void DrawImpulses(void);
      //Get constraint linked to both of these. Seems like a good thing to make general
      //Returns null if there isn't one or if they are connected by a joint
      static MultiContactConstraint* Get(Rigidbody* objectA, Rigidbody* objectB, bool& areJointed, Face* face);

      bool m_isClipPair;
      Manifold m_manifold;
      ContactConstraint m_contacts[MAX_CONTACT_POINTS];
      //A for first tangent axis, B for second
      FrictionConstraint m_frictionsA[MAX_CONTACT_POINTS];
      FrictionConstraint m_frictionsB[MAX_CONTACT_POINTS];

      //Amount of time (in seconds) this constraint can be inactive before it is destroyed
      static float s_timeToRemove;
      //If this is an environment collision this is the face that is colliding.
      //Just used to identify multiple contact constraints between the same two objects for environment collisions
      Face* m_envFace;
    private:
      void SetupJacobian(void);

      float m_timeInactive;
      //Could have up to 12 active constraints, needing 4 matrices. Default only allows 3
      Mat3 m_extraMassMatrix;
    };
  }
}