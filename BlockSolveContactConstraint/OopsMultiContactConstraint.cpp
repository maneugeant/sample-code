#include "OopsMultiContactConstraint.h"
#include "OopsManifold.h"
#include "OopsObject.h"
#include "OopsMLCP.h"
#include "OopsModelAdjInfo.h"//face

namespace Oops
{
  namespace Constraints
  {
    //Don't remove constraint immediately when seperated, keep it around in case they collide again, like in a bounce
    float MultiContactConstraint::s_timeToRemove = 1.0f;

    MultiContactConstraint::MultiContactConstraint(Rigidbody* objA, Rigidbody* objB):
      MultiConstraint(ConstraintType::MultiContact, objA, objB, false), m_timeInactive(0.0f), m_isClipPair(false), m_envFace(nullptr) {}

    MultiContactConstraint::MultiContactConstraint(void):
      MultiConstraint(ConstraintType::MultiContact, 0, 0, false), m_timeInactive(0.0f), m_isClipPair(false), m_envFace(nullptr) {}

    void MultiContactConstraint::DerivedInitialize(void)
    {
      for(int i = 0; i < MAX_CONTACT_POINTS; ++i)
      {
        m_contacts[i].Initialize(m_objA, m_objB, &m_manifold, i);
        m_frictionsA[i].Initialize(m_objA, m_objB, &m_contacts[i], 0, i);
        m_frictionsB[i].Initialize(m_objA, m_objB, &m_contacts[i], 1, i);
      }
      m_envFace = nullptr;
    }

    void MultiContactConstraint::GetMassMatrix(float**& in, unsigned setID)
    {
      if(setID < 3)
        BaseGetMassMatrix(in, setID);
      //Could only be 3, because max constraints is 12, and 12/3 = 4 (setID is 0 based, so 3 would be 4th)
      else
        for(int i = 0; i < 3; ++i)
          in[i] = &m_extraMassMatrix[i][0];
    }

    void MultiContactConstraint::CalculateMass(Constraint** constraints, unsigned setID)
    {
      m_infiniteConstraintMass = false;
      float* ptrs[3];
      float** constraintMass = ptrs;
      GetMassMatrix(constraintMass, setID);
      Mat3 mass;
      float mAB = m_objA->m_invMass + m_objB->m_invMass;

      //The resulting matrix is close to its transpose, so an optimization would be to
      //just calculate the upper diagonal, but this could cause a loss in accuracy.
      //Calculates JT*M*J where J is 3 x 12
      for(unsigned i = 0; i < 3; ++i)
      {
        //Ji*M
        const Vector12& curRow = constraints[i]->GetJacobian().m_preMult;
        constraints[i]->m_noBlockSolve = false;

        for(unsigned j = 0; j < 3; ++j)
        {
          //(Ji*M) * JTj
          const Vector12& otherRow = constraints[j]->GetJacobian();
          mass[i][j] = glm::dot(curRow.m_angularA, otherRow.m_angularA) +
            glm::dot(curRow.m_angularB, otherRow.m_angularB);
        }
        //Linear elements off of the diagoanal are orthogonal,
        //and on the diagonal they are the same, so they dot to 1, so we just need to add masses
        mass[i][i] += mAB;
      }

      //Assumes Lagrangian solver
      HighPrecisionInverse(mass, mass);
      for(int i = 0; i < 3; ++i)
        for(int j = 0; j < 3; ++j)
          constraintMass[i][j] = mass[i][j];
    }

    void MultiContactConstraint::Solve(unsigned iteration)
    {
      //Stick all into blocks of 3 and solve.
      Constraint* all[] = { &m_contacts[0], &m_frictionsA[0], &m_frictionsB[0],
      &m_contacts[1], &m_frictionsA[1], &m_frictionsB[1],
      &m_contacts[2], &m_frictionsA[2], &m_frictionsB[2],
      &m_contacts[3], &m_frictionsA[3], &m_frictionsB[3] };

      for(unsigned i = 0; i < m_manifold.m_noOfContacts; ++i)
      {
        //3 because we block solve in groups of 3, where 1 is contact,
        //and 2 and 3 are the frictions of that same point
        int index = i*3;
        if(all[index]->m_shouldEnforce)
        {
          if(iteration == 1)
          {
            CalculateMass(&all[index], i);
            m_contacts[i].m_posBias = m_contacts[i].GetSloppedPositionBias();
          }

          Vector3 jvb;
          Vector12 velPair(m_objA->m_velocity, m_objA->m_angularVel, m_objB->m_velocity, m_objB->m_angularVel);
          //(-JV-b), but friction doesn't have bias
          for(unsigned j = 0; j < 3; ++j)
          {
            if(m_objA->m_invMass == 0.0f)
              jvb[j] = -all[index + j]->GetJacobian().DotB(velPair);
            else if(m_objB->m_invMass == 0.0f)
              jvb[j] = -all[index + j]->GetJacobian().DotA(velPair);
            else
              jvb[j] = -all[index + j]->GetJacobian().Dot(velPair);
          }

          //Add bias to contact, friction doesn't have bias
          jvb[0] -= m_contacts[i].GetBias();

          //Calculate lambda
          float* ptrs[3];
          float** constraintMass = ptrs;
          GetMassMatrix(constraintMass, i);

          //Get bounds to clamp to for solver
          Vector3 minDelta, maxDelta, lastDelta;
          GetLambdaMinMax(3, &all[index], minDelta, maxDelta);

          Vector3 lambda;
          LagrangianSolver solver(3, constraintMass, &lambda.x, &jvb.x, &maxDelta.x, &minDelta.x);
          solver.Solve();

          AccumulateSums(lambda, &all[index], 3);

          if(!VectorEqual(lambda, Axes::Zero))
            ApplyImpulse(lambda, m_objA, m_objB, &all[index], 3);
        }
      }

      if(iteration == s_maxVelocityIterations)
      {
        //Maybe want to wake up objects penetrating more than slop. Not sure though
        DrawContactInfo();
        DrawImpulses();

        for(unsigned i = 0; i < m_manifold.m_noOfContacts; ++i)
        {
          ContactPoint& c = m_manifold.m_contacts[i];
          c.m_lastContactImpulse = m_contacts[i].m_lambdaSum;
          c.m_lastFrictionImpulseA = m_frictionsA[i].m_lambdaSum;
          c.m_lastFrictionImpulseB = m_frictionsB[i].m_lambdaSum;
        }
      }
    }

    void MultiContactConstraint::SolvePosition(bool setupJacobian, unsigned iteration)
    {
      //For now, I haven't found a cheap enough way to check for collisions after the first iteration, so contacts only get 1
      if(iteration)
        return;
      for(unsigned i = 0; i < m_manifold.m_noOfContacts; ++i)
      {
        ContactConstraint& c = m_contacts[i];
        if(iteration || setupJacobian)
        {
          m_manifold.ResetNormal();
          m_manifold.ResetCenterToContact(i, false, true, false);
          c.m_shouldEnforce = c.ShouldEnforce();
        }
        c.SolvePosition(setupJacobian, iteration);
        //Friction doesn't have position correction, so we don't need to do anything here
      }
    }

    void MultiContactConstraint::AddContact(Manifold& newManifold)
    {
      m_manifold.Lock();
      m_manifold.AddContact(newManifold, m_isClipPair);
      m_manifold.Unlock();
    }

    bool MultiContactConstraint::EnforceContact(ContactPoint& contact)
    {
      //There's an edge case here where existing contacs between two objects where one's group just changed will
      //be enforced when they're not supposed to for a few frames, but forget that.
      return contact.m_penetration > 0.0f;
    }

    void MultiContactConstraint::FirstIteration(void)
    {
      if(m_isClipPair)
      {
        if(m_manifold.m_clipOutOfDate)
          m_manifold.m_noOfContacts = 0;
        //This will be set to false next frame when new contact info is submitted
        m_manifold.m_clipOutOfDate = true;
      }

      //Remains true if all contacts aren't penetrating (or if there aren't any)
      bool isInactive = true;
      float mostPenetration = 0.0f;
      unsigned deepestIndex = 0;

      for(unsigned i = 0; i < m_manifold.m_noOfContacts;)
      {
        //Validate model point distance and penetration for hulls, as old contact points are re-used
        //Continue if a point was discarded, but don't increment, since the last element was put here
        if(m_manifold.ResetCenterToContact(i, m_isClipPair, !m_isClipPair, !m_isClipPair))
          continue;

        //Needs to go after reset because that changes the positions, which could change the penetration
        ContactPoint& curContact = m_manifold.m_contacts[i];
        curContact.m_enforce = m_contacts[i].m_shouldEnforce = m_frictionsA[i].m_shouldEnforce = m_frictionsB[i].m_shouldEnforce = EnforceContact(curContact);

        if(curContact.m_penetration > 0.0f)
          isInactive = false;

        //Find deepest contact and move it to front of contact to converge faster
        if(curContact.m_penetration > mostPenetration)
        {
          deepestIndex = i;
          mostPenetration = curContact.m_penetration;
        }
        ++i;
      }

      if(isInactive)
      {
        m_timeInactive += s_dt;
        if(m_timeInactive > s_timeToRemove)
        {
          m_remove = true;
          return;
        }
      }
      else
        m_timeInactive = 0.0f;

      //Move deepest contact to front for convergence
      //This doesn't affect warm starting because impulses are stored on the contacts, not the constraints
      if(deepestIndex)
        std::swap(m_manifold.m_contacts[0], m_manifold.m_contacts[deepestIndex]);

      //Store restitution bias on constraints, need to do it after loop because they could have changed from deepest swap
      Rigidbody* bodyA = m_manifold.m_bodyA;
      Rigidbody* bodyB = m_manifold.m_bodyB;

      for(unsigned i = 0; i < m_manifold.m_noOfContacts; ++i)
      {
        const ContactPoint& cPoint = m_manifold.m_contacts[i];
        //Velocities at contact points
        Vector3 velPointA = bodyA->m_velocity + glm::cross(bodyA->m_angularVel, cPoint.m_objA.m_centerToContact);
        Vector3 velPointB = bodyB->m_velocity + glm::cross(bodyB->m_angularVel, cPoint.m_objB.m_centerToContact);
        //Relative velocities of those points projected onto the normal
        float projRelVel = glm::dot(velPointB, m_manifold.m_normal) - glm::dot(velPointA, m_manifold.m_normal);
        m_contacts[i].m_velBias = -(m_manifold.m_restitutionCoeff * projRelVel);
        if(abs(m_contacts[i].m_velBias) < ContactConstraint::s_restitutionSlop)
          m_contacts[i].m_velBias = 0.0f;
      }
      //Warm starting happens in MultiConstraint::SetupJacobian
      SetupJacobian();
    }

    void MultiContactConstraint::SetupJacobian(void)
    {
      Constraint* all[] = { &m_contacts[0], &m_frictionsA[0], &m_frictionsB[0],
      &m_contacts[1], &m_frictionsA[1], &m_frictionsB[1],
      &m_contacts[2], &m_frictionsA[2], &m_frictionsB[2],
      &m_contacts[3], &m_frictionsA[3], &m_frictionsB[3] };
      //Times 3 because of 4 contact, 4 frictionA and 4 frictionB, so if there's only 1 contact point, that's 3 constraints
      SetupConstraints(all, m_manifold.m_noOfContacts*3);
    }

    void MultiContactConstraint::DrawContactInfo(void)
    {
      m_manifold.DrawContactInfo();
    }

    void MultiContactConstraint::DrawImpulses(void)
    {
      if(!DRAW_CONTACT_IMPULSES)
        return;
      SetColor(1.0f, 0.0f, 0.0f);
      for(unsigned i = 0; i < m_manifold.m_noOfContacts; ++i)
        DrawVector(m_manifold.m_bodyA->m_owner->ToWorld(m_manifold.m_contacts[i].m_objA.m_modelPoint), 
          m_manifold.m_normal*m_contacts[i].m_lambdaSum);
    }

    MultiContactConstraint* MultiContactConstraint::Get(Rigidbody* objectA, Rigidbody* objectB, bool& areJointed, Face* face)
    {
      if(!objectA || !objectB)
        return nullptr;
      //Need to lock because other threads adding to this list during a search would suck
      objectA->m_constraintEdges.Lock();
      MultiContactConstraint* result = nullptr;
      //Each edge list will contain the other, so choice to use A is arbitrary
      for(auto curEdge = objectA->m_constraintEdges.Begin(); curEdge != objectA->m_constraintEdges.End(); ++curEdge)
        if(curEdge->m_otherBody == objectB)
        {
          if(curEdge->m_constraint->m_type == ConstraintType::MultiContact)
          {
            result = dynamic_cast<MultiContactConstraint*>(curEdge->m_constraint);
            //This constraint is not with the right face, continue
            if(result->m_envFace != face)
            {
              result = nullptr;
              continue;
            }
          }
          else
            areJointed = true;

          objectA->m_constraintEdges.Unlock();
          return result;
        }
      objectA->m_constraintEdges.Unlock();
      return result;
    }
  }
}