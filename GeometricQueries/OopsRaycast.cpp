#include "OopsRaycast.h"
#include "OopsObject.h"
#include "OopsDetection.h"
#include "OopsSimplex.h"
#include "OopsClip.h"

namespace Oops
{

  //Fills m_valid, m_t, m_distSq, and m_id assuming m_point is already valid
  void Populate(HitInfo& result, const Vector3& start, const Vector3& end, PhysicsObj* obj)
  {
    result.m_valid = true;
    result.m_t = GetScalarT(start, end, result.m_point);
    result.m_distSq = glm::distance2(result.m_point, start);
    if(obj)
    {
      //Externally, game thinks collider is just this one for decompositions
      if(obj->m_collider->IsDecomposition())
        result.m_obj = obj->m_parent->GetDecompositionHandle();
      else
        result.m_obj = obj;
      result.m_id = result.m_obj->m_id;
    }
  }

  namespace CollisionDetection
  {

    RayHandler::RayHandler(Collider* obj, const Vector3& rayStart, const Vector3& rayEnd)
    {
      Set(obj, rayStart, rayEnd);
    }

    void RayHandler::Set(Collider* obj, const Vector3& start, const Vector3& end)
    {
      m_a = obj;
      m_b = nullptr;
      m_rayStart = start;
      m_rayEnd = end;
      m_lowerBound = 0.0f;
      m_rayDir = end - start;
      m_model = nullptr;
    }

    Vector3 RayHandler::GetPointInObj(void)
    {
      if(m_a)
        return m_a->m_owner->GetWorldPos();
      //If both are null, it's worth crashing because that shouldn't ever happen
      return m_model->front();
    }

    Vector3 RayHandler::GetObjSupport(const Vector3& dir)
    {
      if(m_a)
        return m_a->GetSupportPoint(dir).second;

      //If both don't exist that's so bad we'll just crash, as this is an internal interface, so it shouldn't happen
      if(m_model->size() == 1)
        return (*m_model)[0];
      size_t bestIndex = 0;
      float bestDot = glm::dot((*m_model)[0], dir);

      for(size_t i = 1; i < m_model->size(); ++i)
      {
        float curDot = glm::dot((*m_model)[i], dir);
        if(curDot > bestDot)
        {
          bestDot = curDot;
          bestIndex = i;
        }
      }
      return (*m_model)[bestIndex];
    }

    //Based on Ray Casting against General Convex Objects with Application to Continuous Collision Detection
    //by GINO VAN DEN BERGEN
    bool RayHandler::LineCast(HitInfo& result)
    {
      Vector3 curNormal;
      //start - Arbitrary point in object
      Vector3 curSearchDir = m_rayStart - GetPointInObj();
      SupportPoint curSupport;
      m_simplex.m_size = 0;
      //A safeguard against infinite loops. Shouldn't happen, but just in case
      unsigned iteration = 0;

      //Arbitrary, non zero value, so loop is entered
      Vector3 closestToOrigin(Axes::Y);

      while(glm::length2(closestToOrigin) > EPSILON && iteration++ < 20)
      {
        Vector3 lowerBoundPoint = GetLowerBoundPoint();

        //Support is in CSO, this is just on the collider
        Vector3 supportOnC = GetObjSupport(curSearchDir);
        curSupport.m_bVertex = supportOnC;
        curSupport.m_point = lowerBoundPoint - supportOnC;
        curSupport.m_bVertex = supportOnC;

        float searchDotSupport = glm::dot(curSearchDir, curSupport.m_point);
        float searchDotRay = glm::dot(curSearchDir, m_rayDir);

        if(searchDotSupport > 0.0f)
        {
          if(searchDotRay >= 0.0)
            return false;

          m_lowerBound -= searchDotSupport/searchDotRay;
          curNormal = curSearchDir;
          Vector3 newLowerBoundPoint = GetLowerBoundPoint();

          //Lower bound updated, translate support points along ray
          for(unsigned i = 0; i < m_simplex.m_size; ++i)
          {
            SupportPoint& support = m_simplex.m_supports[i];
            support.m_point = newLowerBoundPoint - support.m_bVertex;
          }
          //The one that's about to be added needs to be translated too
          curSupport.m_point = newLowerBoundPoint - curSupport.m_bVertex;
        }

        //Add to simplex and evaluate it, removing points in the wrong direction and finding new search direction
        m_simplex.Add(curSupport, true);
        closestToOrigin = m_simplex.GetClosestToOrigin();

        //Simplex contains origin, hit found
        if(m_simplex.Evaluate(curSearchDir))
          break;

        curSearchDir = closestToOrigin;
      }

      result.m_point = GetLowerBoundPoint();
      result.m_normal = glm::normalize(curNormal);
      Populate(result, m_rayStart, m_rayEnd, m_a ? m_a->m_owner : nullptr);
      return true;
    }

    void ShapeCaster::SetObj(Collider* obj)
    { m_a = obj; }

    bool ShapeCaster::ShapeCast(void)
    { return CheckCollision(); }

    SupportPoint ShapeCaster::GetSupport(const Vector3& dir)
    {
      Collider::SupportPair aResult = m_a->GetSupportPoint(dir);
      Collider::SupportPair bResult;
      bResult.first = CollisionDetection::GetSupportPoint(-dir, *m_points);
      bResult.second = (*m_points)[bResult.first];
      return SupportPoint(aResult.first, aResult.second, bResult.first, bResult.second);
    }

    ClosestParams::ClosestParams(PhysicsObj& obj)
    {
      m_objPos = SIMD::LoadVector3(obj.GetWorldPos());
      m_objScale = SIMD::LoadVector3(obj.m_scale);
      Mat3 rot = obj.GetWorldRot();
      m_objRot = SIMD::Mat3To4(rot);
      m_primaryAxis = SIMD::LoadVector3(rot[obj.m_collider->GetPrimaryAxis()]);
      m_secondaryAxis = SIMD::LoadVector3(rot[obj.m_collider->GetSecondaryAxis()]);
    }

    SupportPoint ClosestPointer::GetSupport(const Vector3& dir)
    {
      Collider::SupportPair aResult = m_a->GetSupportPoint(dir);
      return SupportPoint(aResult.first, aResult.second, -1, m_point);
    }

    Vector3 ClosestPointer::GetClosestOnCollider(Collider* obj, const Vector3& point)
    {
      m_a = obj;
      m_point = point;
      //Slow conversions, but just for convienence of caller. If he wanted fast he wouldn't called the SIMD version
      m_params = ClosestParams(*obj->m_owner);
      m_params.m_sPoint = SIMD::LoadVector3(point);
      m_params.m_objPos = SIMD::LoadVector3(obj->m_owner->GetWorldPos());
      m_params.m_objScale = SIMD::LoadVector3(obj->m_owner->m_scale);
      m_params.m_objRot = SIMD::Mat3To4(obj->m_owner->GetWorldRot());

      switch(obj->m_model->m_shapeType)
      {
      case ShapeType::Box: return SIMD::GetVector3(GetClosestOnCube());
      case ShapeType::Sphere: return SIMD::GetVector3(GetClosestOnSphere());
      case ShapeType::Cylinder: return SIMD::GetVector3(GetClosestOnCylinder());
      case ShapeType::Capsule: return SIMD::GetVector3(GetClosestOnCapsule());
      default: return GetClosestOnPolygon();
      }
    }

    SIMD::SVector4 ClosestPointer::GetClosestOnCollider(Collider* obj, const ClosestParams& params)
    {
      m_a = obj;
      m_params = params;

      switch(obj->m_model->m_shapeType)
      {
      case ShapeType::Box: return GetClosestOnCube();
      case ShapeType::Sphere: return GetClosestOnSphere();
      case ShapeType::Cylinder: return GetClosestOnCylinder();
      case ShapeType::Capsule: return GetClosestOnCapsule();
      //Only cloth uses the SIMD version and it won't call it on these types
      default: return SIMD::SVector4();//GetClosestOnPolygon();
      }
    }

    SIMD::SVector4 ClosestPointer::GetClosestOnCube(void)
    {
      //Sphere's position in box's frame of reference
      SIMD::SVector4 localSphere = SIMD::Transform(SIMD::Transpose(m_params.m_objRot), m_params.m_sPoint - m_params.m_objPos);
      SIMD::SVector4 bScale2 = m_params.m_objScale*0.5f;

      //Get closest point on box to sphere
      localSphere = SIMD::Clamp(localSphere, -bScale2, bScale2);
      //Transform back to world
      return SIMD::Transform(m_params.m_objRot, localSphere) + m_params.m_objPos;
    }

    SIMD::SVector4 ClosestPointer::GetClosestOnSphere(void)
    {
      SIMD::SVector4 toPoint = m_params.m_sPoint - m_params.m_objPos;
      //This could be optimized to not use floats and just simd vectors, maybe worth revisiting later
      float pointDist = SIMD::Length(toPoint);
      float radius = m_a->m_owner->m_scale.x*0.5f;
      //Point is closer than radius, meaning it is inside sphere
      if(pointDist <= radius)
        return m_params.m_sPoint;
      //Return point on boundary of sphere closest to desired point, since point isn't in sphere
      return m_params.m_objPos + toPoint*(1.0f/pointDist)*radius;
    }

    SIMD::SVector4 ClosestPointer::ClampToCircle(float radius, SIMD::SVector4 circleCenter)
    {
      //Project point onto circle surface
      //In the cases this is used primary axis is always the normal of the circle
      SIMD::SVector4 pointOnCircle = SIMD::PlaneProj(m_params.m_sPoint, circleCenter, m_params.m_primaryAxis);

      //Now that it's on the surface, clamp it to within the circle
      SIMD::SVector4 circleToPoint = pointOnCircle - circleCenter;
      SIMD::SVector4 toCircleBorder = SIMD::Normalized(circleToPoint)*radius;
      //Actual clamp happens here.
      return circleCenter + SIMD::ShortestAlongVector(circleToPoint, toCircleBorder, circleToPoint);
    }

    SIMD::SVector4 ClosestPointer::GetClosestOnCylinder(void)
    {
      //Project along primary axis and clamp to height
      SIMD::SVector4 toPoint = m_params.m_sPoint - m_params.m_objPos;
      float pointAlongAxis = SIMD::DotFloat(toPoint, m_params.m_primaryAxis);
      SIMD::SVector4 pointOnCylinder;

      //Clamp to height
      float cylinderHalfLength = m_a->m_owner->m_scale[m_a->GetPrimaryAxis()]*0.5f;
      if(pointAlongAxis > cylinderHalfLength)
        pointOnCylinder = m_params.m_objPos + m_params.m_primaryAxis*cylinderHalfLength;
      else if(pointAlongAxis < -cylinderHalfLength)
        pointOnCylinder = m_params.m_objPos - m_params.m_primaryAxis*cylinderHalfLength;
      else
        pointOnCylinder = m_params.m_objPos + m_params.m_primaryAxis*pointAlongAxis;

      float circleRadius = m_a->m_owner->m_scale[m_a->GetSecondaryAxis()]*0.5f;
      return ClampToCircle(circleRadius, pointOnCylinder);
    }

    SIMD::SVector4 ClosestPointer::GetClosestOnCapsule(void)
    {
      //Project along primary axis and clamp to height
      SIMD::SVector4 toPoint = m_params.m_sPoint - m_params.m_objPos;
      float pointAlongAxis = SIMD::DotFloat(toPoint, m_params.m_primaryAxis);
      SIMD::SVector4 pointOnCylinder;
      float circleRadius = m_a->m_owner->m_scale[m_a->GetSecondaryAxis()]*0.5f;

      //Clamp to height
      float cylinderHalfLength = m_a->m_owner->m_scale[m_a->GetPrimaryAxis()]*0.5f;
      if(pointAlongAxis > cylinderHalfLength)
        pointOnCylinder = m_params.m_objPos + m_params.m_primaryAxis*cylinderHalfLength;
      else if(pointAlongAxis < -cylinderHalfLength)
        pointOnCylinder = m_params.m_objPos - m_params.m_primaryAxis*cylinderHalfLength;
      else
      {
        pointOnCylinder = m_params.m_objPos + m_params.m_primaryAxis*pointAlongAxis;
        //Point is within cylinder portion, just clamp to circle portion, not spheres
        return ClampToCircle(circleRadius, pointOnCylinder);
      }

      //We clamped to cylider, now clamp to spheres on end
      SIMD::SVector4 tipToPoint = m_params.m_sPoint - pointOnCylinder;
      SIMD::SVector4 tipToBoundary = SIMD::Normalized(tipToPoint)*circleRadius;
      return pointOnCylinder + SIMD::ShortestAlongVector(tipToPoint, tipToBoundary, tipToPoint);
    }

    Vector3 ClosestPointer::GJKClosestPoint(void)
    {
      m_simplex.Clear();

      //Arbitrary choice for first support
      Vector3 curSearchDir = Axes::Y;
      SupportPoint newPoint(GetSupport(curSearchDir));

      //Search until result is found, either collision or not
      unsigned iterations = 0;
      while(iterations++ < GJK_TERMINATION_ITERATION)
      {
        //Add to simplex and evaluate it, removing points in the wrong direction and finding new search direction
        m_simplex.Add(newPoint);
        //If curSearchDir is zero (from evaluate changing it), then that means the origin lies on the edge of the simplex
        //This resulted in a cross product with two parallel vectors, leading to 0
        if(m_simplex.Evaluate(curSearchDir))
        {
          m_simplex.Draw();
          //Simplex contains origin so the two shapes intersect, so distance is 0,
          //and closest point to origin is the origin itself
          return Axes::Zero;
        }

        //Get new simplex point in direction evaluated by last loop
        newPoint = GetSupport(curSearchDir);

        //This is the only termination check I have found to work for evaluating distance
        //I've seen testing to see if new simplex is closer than old one, but this sometimes terminates early
        //I've seen testing to see if new support is further along support direction than old closest point, but this sometimes terminates late
        //This one is concerning for mathematically defined shapes that could generate many very close support points,
        //but I have special cases for most of them, so hopefully this is good enough
        if(m_simplex.IsSimplexPoint(newPoint))
          return m_simplex.GetClosestToOrigin();

        //Uncomment for debug drawing next search direction and support point
        /*if(iterations + 1 > EPA_TERMINATION_ITERATION)
        {
          DrawVector(m_a->m_owner->GetWorldPos(), SafeNormalize(curSearchDir));
          DrawSphere(newPoint.m_point + newPoint.m_bVertex, 0.1f);
        }*/
      }
      //Iteration cap reached, rare edge case likely due to floating point error, or a bug in this algorithm
      return m_simplex.GetClosestToOrigin();
    }

    Vector3 ClosestPointer::GetClosestOnPolygon(void)
    {
      Vector3 result = GJKClosestPoint();
      //Adding point takes it out of configuration space and back into world, since support is obj - point
      //If point is inside object, result will be the origin, and point will be returned
      return result + m_point;
    }
  }//CollisionDetection

  bool Raycaster::LineCastSphere(const Vector3& start, const Vector3& end, PhysicsObj& obj, HitInfo& result)
  {
    //Reject if start of line is inside sphere
    float radius = obj.m_scale.x*0.5f;
    Vector3 objWorldPos = obj.GetWorldPos();
    if(glm::distance2(start, objWorldPos) <= radius*radius)
      return false;

    Vector3 hitPoint;
    if(!GetLineSphereIntersection(objWorldPos, radius, start, end, hitPoint))
      return false;

    result.m_point = hitPoint;
    //User knows normal isn't normalized
    result.m_normal = result.m_point - objWorldPos;
    Populate(result, start, end, &obj);
    return true;
  }

  bool Raycaster::LineCastCapsule(const Vector3& start, const Vector3& end, PhysicsObj& obj, HitInfo& result, bool isCylinder)
  {
    Vector3 hit;
    unsigned primaryAxis = obj.m_collider->GetPrimaryAxis();
    unsigned otherAxis = primaryAxis + 1;
    if(otherAxis > 2)
      otherAxis = 0;
    float nonPrimaryScale = obj.m_scale[otherAxis];
    Vector3 scaledY = obj.GetWorldRot()[primaryAxis]*obj.m_scale[primaryAxis]*0.5f;
    Vector3 objPos = obj.GetWorldPos();
    Vector3 endA = objPos + scaledY;
    Vector3 endB = objPos - scaledY;

    if(!GetLineCapsuleIntersection(endA, endB, nonPrimaryScale*0.5f, start, end, result.m_point, result.m_normal, isCylinder))
      return false;

    Populate(result, start, end, &obj);
    return true;
  }

  bool Raycaster::LineCastBox(const Vector3& start, const Vector3& end, PhysicsObj& obj, HitInfo& result)
  {
    Vector3 localStart = obj.InvTransRot(start);
    Vector3 scale2 = obj.m_scale/2.0f;

    //If it starts inside the box, discard it
    if(abs(localStart.x) < scale2.x && abs(localStart.y) < scale2.y && abs(localStart.z) < scale2.z)
      return false;

    Vector3 localEnd = obj.InvTransRot(end);
    float t = -1.0f;

    if(!AABBLineIntersect(localStart, localEnd, AABB(-scale2, scale2), &t))
      return false;

    Vector3 localHit = Lerp(localStart, localEnd, t);
    //Get most significant absolute axis, as that will be the normal
    //This only works if scale is removed from the calculation, 
    //otherwise long skinny objects would be bias on the small faces
    int highestAxis = MostSignificantAxis(Scale(localHit, Inverse(scale2)));
    Vector3 localNormal;
    localNormal[highestAxis] = 1.0f;
    if(localHit[highestAxis] < 0.0f)
      localNormal *= -1.0f;

    result.m_point = obj.RotTrans(localHit);
    result.m_normal = obj.GetWorldRot() * localNormal;
    Populate(result, start, end, &obj);
    return true;
  }


  bool Raycaster::LineCast(const Vector3& start, const Vector3& end, PhysicsObj& obj, HitInfo& result)
  {
    if(!obj.m_collider)
      return false;

    switch(obj.m_collider->m_model->m_shapeType)
    {
    case ShapeType::Sphere:
      return LineCastSphere(start, end, obj, result);
    case ShapeType::Capsule:
      return LineCastCapsule(start, end, obj, result, false);
    case ShapeType::Cylinder:
      return LineCastCapsule(start, end, obj, result, true);
    case ShapeType::Box:
      return LineCastBox(start, end, obj, result);
    }

    m_raycaster.Set(obj.m_collider, start, end);
    m_raycaster.LineCast(result);
    return result.m_valid;
  }

  bool Raycaster::LineCast(const Vector3& start, const Vector3& end, const std::vector<Vector3>* model, HitInfo& result)
  {
    m_raycaster.Set(nullptr, start, end);
    m_raycaster.SetModel(model);
    m_raycaster.LineCast(result);
    return result.m_valid;
  }

  Vector3 GetSupport(PhysicsObj& obj, const Vector3& dir)
  { return obj.m_collider->m_supportGraph.GetSupportPoint(dir).second; }

  bool SortHelper(const HitInfo& lhs, const HitInfo& rhs)
  { return lhs.m_t < rhs.m_t; }

  void Raycaster::SortResults(void)
  { std::sort(m_results.begin(), m_results.end(), &SortHelper); }
}