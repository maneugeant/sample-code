#pragma once
#include "OopsRaycastContainers.h"
#include "OopsDetection.h"

namespace Oops
{
  //Fills m_valid, m_t, m_distSq, and m_id assuming m_point is already valid
  void Populate(HitInfo& result, const Vector3& start, const Vector3& end, PhysicsObj* obj);

  namespace CollisionDetection
  {
    class RayHandler : public CollisionHandler
    {
    public:
      RayHandler(Collider* obj, const Vector3& rayStart, const Vector3& rayEnd);
      RayHandler(void): m_lowerBound(0.0f), m_model(nullptr) {}

      //Only model or collider is used, so if one is specified, invalidate the other
      void SetObj(Collider* obj) { m_a = obj; m_model = nullptr; }
      void SetModel(const std::vector<Vector3>* model) { m_model = model; m_a = nullptr; }
      void Set(Collider* obj, const Vector3& start, const Vector3& end);

      bool LineCast(HitInfo& result);

      Vector3 GetLowerBoundPoint(void) { return m_rayStart + m_rayDir*m_lowerBound; }
      //Get support point on the object being casted at
      Vector3 GetObjSupport(const Vector3& dir);
      Vector3 GetPointInObj(void);

      Vector3 m_rayStart;
      Vector3 m_rayDir;
      //Current lowest scalar of rayDir that is known to be outside or on the boundary
      float m_lowerBound;
      Vector3 m_rayEnd;
      //If no collider is given, this is used for support points instead
      const std::vector<Vector3>* m_model;
    };

    class ShapeCaster : public CollisionHandler
    {
    public:
      ShapeCaster(void) { m_a = 0; }
      //Uses pointer, so don't let it out of scope while using this
      ShapeCaster(Collider* obj, std::vector<Vector3>* points): m_points(points) { m_a = obj; }
      void SetObj(Collider* obj);
      SupportPoint GetSupport(const Vector3& dir);
      bool ShapeCast(void);

      std::vector<Vector3>* m_points;
    };

    struct SIMD_ALIGN ClosestParams
    {
      ClosestParams(PhysicsObj& obj);
      ClosestParams(void) {}

      SIMD::SVector4 m_sPoint;
      SIMD::SVector4 m_objPos;
      SIMD::SVector4 m_objScale;
      SIMD::SVector4 m_primaryAxis;
      SIMD::SVector4 m_secondaryAxis;
      SIMD_ALIGN SIMD::SMatrix4 m_objRot;
    };

    //Finds closest point on collider to point
    class SIMD_ALIGN ClosestPointer : public CollisionHandler
    {
    public:
      SupportPoint GetSupport(const Vector3& dir);
      Vector3 GetClosestOnCollider(Collider* obj, const Vector3& point);
      SIMD::SVector4 GetClosestOnCollider(Collider* obj, const ClosestParams& params);
    private:
      Vector3 GJKClosestPoint(void);
      Vector3 GetClosestOnPolygon(void);
      SIMD::SVector4 GetClosestOnCube(void);
      SIMD::SVector4 GetClosestOnSphere(void);
      SIMD::SVector4 GetClosestOnCylinder(void);
      SIMD::SVector4 GetClosestOnCapsule(void);

      SIMD::SVector4 ClampToCircle(float radius, SIMD::SVector4 circleCenter);

      Vector3 m_point;
      SIMD_ALIGN ClosestParams m_params;
    };
  }

  class Raycaster
  {
  public:
    bool LineCast(const Vector3& start, const Vector3& end, PhysicsObj& obj, HitInfo& result);
    bool LineCast(const Vector3& start, const Vector3& end, const std::vector<Vector3>* model, HitInfo& result);
    bool LineCastSphere(const Vector3& start, const Vector3& end, PhysicsObj& obj, HitInfo& result);
    bool LineCastCapsule(const Vector3& start, const Vector3& end, PhysicsObj& obj, HitInfo& result, bool isCylinder);
    bool LineCastBox(const Vector3& start, const Vector3& end, PhysicsObj& obj, HitInfo& result);
    bool Raycast(const Ray& ray, PhysicsObj& obj, HitInfo& result);
    void SortResults(void);
    bool ShapeCast(Collider* obj)
    { m_shapeCaster.SetObj(obj); return m_shapeCaster.ShapeCast(); }

    std::vector<HitInfo> m_results;
    CollisionDetection::ShapeCaster m_shapeCaster;
    CollisionDetection::RayHandler m_raycaster;

    const static int MAX_RAY_ITERATIONS = 20;
  };
}