//Generates traversal functions to visit neighboring nodes during astar without needing if statements
//This is done by computing neighbors that should be visited and putting that in a bitfield which is used
//as an index into an array of traversal functions that conver all possibilities in a grid
#include "TraversalGenerator.h"
#include "TerrainNode.h"
#include <string>
#include <iostream>
#include <fstream>

#ifdef GENERATING_TRAVERSAL
void (*g_traversalFuncs[256])(TerrainNode*, AStar*);
#endif

//Make functions like this, excluding traversal calls if that neighbor doesn't need to be visited
/*void TraverseNode(TerrainNode* curNode, AStar* astar)
{
  int index = astar->GetIndex1D(curNode);
  //southwest
  astar->HandleChild(curNode, astar->Get(index - (MAX_GRID_WIDTH + 1)), true);
  //west
  astar->HandleChild(curNode, astar->Get(index - 1), false);
  //northwest
  astar->HandleChild(curNode, astar->Get(index + (MAX_GRID_WIDTH - 1)), true);
  //south
  astar->HandleChild(curNode, astar->Get(index - MAX_GRID_WIDTH), false);
  //southeast
  astar->HandleChild(curNode, astar->Get((index - MAX_GRID_WIDTH) + 1), true);
  //east
  astar->HandleChild(curNode, astar->Get(index + 1), false);
  //northeast
  astar->HandleChild(curNode, astar->Get(index + MAX_GRID_WIDTH + 1), true);
  //north
  astar->HandleChild(curNode, astar->Get(index + MAX_GRID_WIDTH), false);
}*/

void GenerateTraversalFunctions(void)
{
  std::string voidFunc = "void ";
  std::string baseFuncName = "TraverseNode";
  std::string funcOpen = 
R"((TerrainNode* curNode, AStar* astar)
{
  int index = astar->GetIndex1D(curNode);
  float parentCost = curNode->GetCostToOrigin();
  unsigned char parentX = curNode->m_x;
  unsigned char parentY = curNode->m_y;
)";
  std::string funcEnd = "}\n";
  std::string sw = "  astar->HandleChild(curNode, astar->Get(index - (astar->m_width + 1)), parentCost, DIAG_COST, parentX, parentY);\n";
  std::string w = "  astar->HandleChild(curNode, astar->Get(index - 1), parentCost, ORTHO_COST, parentX, parentY);\n";
  std::string nw = "  astar->HandleChild(curNode, astar->Get(index + (astar->m_width - 1)), parentCost, DIAG_COST, parentX, parentY);\n";
  std::string s = "  astar->HandleChild(curNode, astar->Get(index - astar->m_width), parentCost, ORTHO_COST, parentX, parentY);\n";
  std::string se = "  astar->HandleChild(curNode, astar->Get((index - astar->m_width) + 1), parentCost, DIAG_COST, parentX, parentY);\n";
  std::string e = "  astar->HandleChild(curNode, astar->Get(index + 1), parentCost, ORTHO_COST, parentX, parentY);\n";
  std::string ne = "  astar->HandleChild(curNode, astar->Get(index + astar->m_width + 1), parentCost, DIAG_COST, parentX, parentY);\n";
  std::string n = "  astar->HandleChild(curNode, astar->Get(index + astar->m_width), parentCost, ORTHO_COST, parentX, parentY);\n";

  std::fstream out("Traversal.cpp", std::ios_base::out);
  out << 
R"(
#include "TerrainNode.h"
#include "Astar.h"

void TraverseNode0(TerrainNode*, AStar*) {}

)";



  for(int i = 1; i < 256; ++i)
  {
    out << voidFunc + baseFuncName << i << funcOpen;
    //Order these so they're done left to right on rows, to possibly avoid extra cache misses
    if(Flags::NeighborW & i)
      out << w;
    if(Flags::NeighborE & i)
      out << e;
    if(Flags::NeighborN & i)
      out << n;
    if(Flags::NeighborNE & i)
      out << ne;
    if(Flags::NeighborNW & i)
      out << nw;
    if(Flags::NeighborSE & i)
      out << se;
    if(Flags::NeighborS & i)
      out << s;
    if(Flags::NeighborSW & i)
      out << sw;
    out << funcEnd;
  }

  out << "void (*g_traversalFuncs[256])(TerrainNode*, AStar*) =\n{ ";
  out << "&" << baseFuncName << 0 << "\n";
  for(int i = 1; i < 256; ++i)
    out << "  ,&" << baseFuncName << i << "\n";
  out << "};";
}