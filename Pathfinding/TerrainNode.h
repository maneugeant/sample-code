#pragma once
//#define TESTING_ASTAR
//sqrt(2)
#define DIAG_COST 1.41421356237f
#define ORTHO_COST 1.0f
#define OCTILE_SCALAR 0.41421356237f

namespace Flags
{
  enum
  {
    NeighborN = 1 << 0,
    NeighborNE = 1 << 1,
    NeighborE = 1 << 2,
    NeighborSE = 1 << 3,
    NeighborS = 1 << 4,
    NeighborSW = 1 << 5,
    NeighborW = 1 << 6,
    NeighborNW = 1 << 7,
  };

  enum
  {
    InOpenList = 1 << 0,
    InClosedList = 1 << 1,
    IsWall = 1 << 2,
    //Relative position of parent from this node
    ParentN = 1 << 3,
    ParentE = 1 << 4,
    ParentS = 1 << 5,
    ParentW = 1 << 6,
    Altered = 1 << 7,
  };
}

class AStar;

class TerrainNode
{
public:
  TerrainNode(void): m_next(nullptr), m_prev(nullptr), m_nextAltered(nullptr),
    m_flags(0), m_multiplier(1.0f), m_costToOrigin(0.0f), m_parentX(0), m_parentY(0) {}

  static void InsertOpenList(TerrainNode*& rootOpen, TerrainNode*& tailOpen, TerrainNode* toAdd);
  static void RemoveOpenList(TerrainNode*& rootOPen, TerrainNode*& tailOpen, TerrainNode* toRemove);
  static TerrainNode* PopFrontOpenList(TerrainNode*& rootOpen, TerrainNode*& tailOpen);
  static void AddToAltered(TerrainNode*& rootAltered, TerrainNode* toAdd);
  static void ClearAllAltered(TerrainNode*& rootAltered);

  void Clear(void);

  void SetCost(float costToOrigin, float costToGoal, float weight);
  void SetCost(float costToGoal, float weight);
  void SetCostToOrigin(float costToOrigin);
  float GetCostToGoal(float weight);
  float GetCostToOrigin(void);
  float GetCostTotal(void);
  unsigned GetUnsignedCost(void);
  void SetIsWall(bool value);
  bool GetIsWall(void);
  void SetClosed(bool value);
  bool GetClosed(void);
  unsigned GetFlags(void);
  void GetPos(int& x, int& y);
  void SetPos(int x, int y);
  TerrainNode* GetNextAltered(void);
  bool IsClear(void);
  void SetApproach(TerrainNode* n, TerrainNode* e, TerrainNode* s, TerrainNode* w, TerrainNode* ne, TerrainNode* nw, TerrainNode* se, TerrainNode* sw);
  float& GetNodeWeight(void) { return m_multiplier; }

  static const unsigned AllDirectionFlags;
  static const unsigned AllListFlags;
  static const unsigned AllNeighbors;

  unsigned char m_traversalFunc;
  //It is possible to implicitly calculate these, but makes things more complicated and only saves 2 bytes
  unsigned char m_x;
  unsigned char m_y;
  unsigned char m_parentX;
  unsigned char m_parentY;
private:
  void SetFlags(unsigned flags);
  void ClearFlags(unsigned flags);

  static TerrainNode* FindForward(TerrainNode* start, float cost);
  static TerrainNode* FindBackward(TerrainNode* start, float cost);

  //Next in open list sorted by increasing cost
  TerrainNode* m_next;
  TerrainNode* m_prev;
  //Next in singly-linked list of nodes altered by current search
  TerrainNode* m_nextAltered;
  float m_totalCost;
  float m_costToOrigin;
  //Multiplier used to weight nodes
  float m_multiplier;
  unsigned char m_flags;
};