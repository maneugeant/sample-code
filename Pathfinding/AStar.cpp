#include "AStar.h"

#include "TraversalGenerator.h"
#include <vector>
#include <utility>
#include <algorithm>

#undef max
#undef min

AStar::AStar(size_t width, size_t height): m_width(width), m_height(height), m_alteredList(nullptr), m_openList(nullptr), m_openTail(nullptr), m_heuristic(Heuristic::Octile)
{
  m_grid.resize(width*height);
  for(size_t y = 0; y < m_height; ++y)
    for(size_t x = 0; x < m_width; ++x)
      Get(x, y)->SetPos(x, y);
  PreprocessMap();
  SetHeuristic(Heuristic::Octile);
}

void AStar::AssertNoAltered(void)
{
  for(auto& node : m_grid)
    if(node.GetFlags() & TerrainNode::AllListFlags)
      __debugbreak();
}

void AStar::ClearGrid(void)
{
  for(TerrainNode& node : m_grid)
    node.Clear();
}

TerrainNode* AStar::Get(int x, int y)
{
  return &m_grid[x + y*m_width];
}

TerrainNode* AStar::SafeGet(int x, int y)
{
  size_t index = x + y*m_width;
  if(index < m_width*m_height)
    return &m_grid[index];
  return nullptr;
}

TerrainNode* AStar::Get(int index)
{
  return &m_grid[index];
}

int AStar::GetIndex1D(TerrainNode* node)
{
  return node - &m_grid[0];
}

void AStar::SetIsWall(int x, int y, bool value)
{
  Get(x, y)->SetIsWall(value);
}

void AStar::SetNodeWeight(int x, int y, float value)
{
  Get(x, y)->GetNodeWeight() = value;
}

//Conservative test to see if you can simplify the path, tests if a wall is within the bounding box createx by the 4 params
bool AStar::IsPathClear(int startX, int startY, int endX, int endY)
{
  int minX, minY, maxX, maxY;
  minX = std::min(startX, endX);
  minY = std::min(startY, endY);
  maxX = std::max(startX, endX);
  maxY = std::max(startY, endY);

  for(int x = minX; x <= maxX; ++x)
    for(int y = minY; y <= maxY; ++y)
      if(Get(x, y)->GetIsWall())
        return false;
  return true;
}

extern int g_openSize;

int AStar::StartPath(TerrainNode* start, TerrainNode* end, float weight)
{
  if(!start || !end)
    return PathResult::Fail;

  m_goalNode = end;
  m_startNode = start;
  int startX = m_startNode->m_x;
  int startY = m_startNode->m_y;
  m_goalX = m_goalNode->m_x;
  m_goalY = m_goalNode->m_y;

  //Reset state of altered nodes from last query
  if(m_alteredList)
    m_alteredList->ClearAllAltered(m_alteredList);
  g_openSize = 0;

  //Straight line optimization: if there aren't any walls in the bounding box of this path, just walk straight there, don't bother with astar
  if(IsPathClear(startX, startY, m_goalX, m_goalY))
  {
    m_resultPath.resize(2);
    m_resultPath[0] = { static_cast<float>(startX), static_cast<float>(startY) };
    m_resultPath[1] = { static_cast<float>(m_goalX), static_cast<float>(m_goalY) };
    return PathResult::Success;
  }

  m_weight = weight;
  m_curStatus = PathResult::Working;

  m_openList = m_openTail = m_startNode;
  TerrainNode::AddToAltered(m_alteredList, m_startNode);
  return ContinuePath();
}

int AStar::StartPath(int startX, int startY, int goalX, int goalY, float weight)
{
  TerrainNode* start = SafeGet(startX, startY);
  TerrainNode* end = SafeGet(goalX, goalY);
  return StartPath(start, end, weight);
}

int AStar::ContinuePath()
{
  while(m_openList)
  {
    TerrainNode* curNode = GetOpenNode();
    if(curNode == m_goalNode)
    {
      m_curStatus = PathResult::Success;
      break;
    }

    //Precomputed visitor functions to visit all neighbors that aren't walls to avoid if statements
    g_traversalFuncs[curNode->m_traversalFunc](curNode, this);

    AddToClosed(curNode);

    if(m_curStatus != PathResult::Working)
      break;
  }

  if(m_curStatus == PathResult::Success)
  {
    StoreResultPath(m_goalNode);
    return PathResult::Success;
  }

  return PathResult::Fail;
}

//Called from auto-generated traversal functions
void AStar::HandleChild(TerrainNode* parent, TerrainNode* child, float parentCost, float stepCost, unsigned char parentX, unsigned char parentY)
{
  int childFlags = child->GetFlags();
  TerrainNode::AddToAltered(m_alteredList, child);

  float newCost = parentCost + stepCost*child->GetNodeWeight();

  //If it isn't in a list, put it in the open list
  if(!(childFlags & TerrainNode::AllListFlags))
  {
    child->SetCostToOrigin(newCost);
    child->m_parentX = parentX;
    child->m_parentY = parentY;
    AddToOpen(child);
  }
  else
  {
    //If this path to the node is better than the existing one, update it
    float oldCost = child->GetCostToOrigin();
    if(newCost < oldCost)
    {
      if(child->GetFlags() & Flags::InOpenList)
        RemoveFromOpen(child);
      child->SetCostToOrigin(newCost);
      child->m_parentX = parentX;
      child->m_parentY = parentY;
      AddToOpen(child);
    }
  }
}

void AStar::AddToOpen(TerrainNode* node)
{
  (this->*m_costFunc)(node);
  TerrainNode::InsertOpenList(m_openList, m_openTail, node);
}

void AStar::AddToClosed(TerrainNode* node)
{
  node->SetClosed(true);
}

void AStar::RemoveFromOpen(TerrainNode* node)
{
  TerrainNode::RemoveOpenList(m_openList, m_openTail, node);
}

void AStar::ComputeCostOctile(TerrainNode* node)
{
  int curX, curY;
  node->GetPos(curX, curY);
  int diffX = std::abs(m_goalX - curX);
  int diffY = std::abs(m_goalY - curY);

  //1.41*minXY + maxXY - minXY
  //0.41*minXY + maxXY
  float octileCost;
  if(diffX > diffY)
    octileCost = diffY*OCTILE_SCALAR + diffX;
  else
    octileCost = diffX*OCTILE_SCALAR + diffY;
  node->SetCost(octileCost, m_weight);
}

void AStar::ComputeCostSwitch(TerrainNode* node)
{
  int curX, curY;
  node->GetPos(curX, curY);
  int diffX = std::abs(m_goalX - curX);
  int diffY = std::abs(m_goalY - curY);

  float costToGoal;
  switch(m_heuristic)
  {
  case Heuristic::Euclidean:
    costToGoal = sqrt(static_cast<float>(diffX*diffX + diffY*diffY));
    break;
  case Heuristic::Manhattan:
    costToGoal = static_cast<float>(diffX + diffY);
    break;
  case Heuristic::Chebyshev:
    costToGoal = static_cast<float>(std::max(diffX, diffY));
    break;
  case Heuristic::Octile:
    return ComputeCostOctile(node);
  }
  node->SetCost(node->GetCostToOrigin(), costToGoal, m_weight);
}

TerrainNode* AStar::GetParent(TerrainNode* node)
{
  if(node->m_parentX == node->m_x && node->m_parentY == node->m_y)
    return nullptr;
  return Get(static_cast<int>(node->m_parentX), static_cast<int>(node->m_parentY));
}

TerrainNode* AStar::GetOpenNode(void)
{
  return TerrainNode::PopFrontOpenList(m_openList, m_openTail);
}

void AStar::StoreResultPath(TerrainNode* end)
{
  while(end)
  {
    m_resultPath.push_back({ static_cast<float>(end->m_x), static_cast<float>(end->m_y)});
    end = GetParent(end);
  }
}

const std::vector<Point>& AStar::GetResultPath(void)
{
  return m_resultPath;
}

bool IsWall(TerrainNode* a, TerrainNode* b, TerrainNode* c) { return a->GetIsWall() && b->GetIsWall() && c->GetIsWall(); }

bool IsWall(TerrainNode* a, TerrainNode* b) { return a->GetIsWall() && b->GetIsWall(); }

int GetNumWalls(TerrainNode* n, TerrainNode* e, TerrainNode* s, TerrainNode* w, TerrainNode* ne, TerrainNode* nw, TerrainNode* se, TerrainNode* sw)
{
  int result = 0;
  if(n->GetIsWall()) ++result;
  if(e->GetIsWall()) ++result;
  if(s->GetIsWall()) ++result;
  if(w->GetIsWall()) ++result;
  if(ne->GetIsWall()) ++result;
  if(nw->GetIsWall()) ++result;
  if(se->GetIsWall()) ++result;
  if(sw->GetIsWall()) ++result;
  return result;
}

void AStar::PreprocessMap(void)
{
  for(TerrainNode& node : m_grid)
  {
    int x, y;
    node.GetPos(x, y);
    TerrainNode* n = SafeGet(x, y + 1);
    TerrainNode* e = SafeGet(x + 1, y);
    TerrainNode* s = SafeGet(x, y - 1);
    TerrainNode* w = SafeGet(x - 1, y);

    TerrainNode* ne = SafeGet(x + 1, y + 1);
    TerrainNode* nw = SafeGet(x - 1, y + 1);
    TerrainNode* se = SafeGet(x + 1, y - 1);
    TerrainNode* sw = SafeGet(x - 1, y - 1);

    node.SetApproach(n, e, s, w, ne, nw, se, sw);
  }
}

void AStar::InsertResult(const Point& point, size_t index)
{
  m_resultPath.insert(m_resultPath.begin() + index, point);
}

void AStar::RemoveResult(size_t index)
{
  m_resultPath.erase(m_resultPath.begin() + index);
}

void AStar::RubberBand(void)
{
  if(m_resultPath.empty())
    return;

  //-3 because we evaluate 3 at a time, and -1 more because there's a null element at the end
  for(size_t i = 0; i + 3 < m_resultPath.size(); ++i)
  {
    Point start = m_resultPath[i];
    Point end = m_resultPath[i + 2];
    //If path from inded 0 to 2 is clear, then 1 is redundant, so remove it
    //Incrementing i works out even with removal, since removal will shift the node we were on to the left, so incrementing i gets us back to it
    if(IsPathClear(static_cast<int>(start.x), static_cast<int>(start.y), static_cast<int>(end.x), static_cast<int>(end.y)))
      RemoveResult(i + 1);
  }
}

void AStar::MakeEquidistant(void)
{
  if(m_resultPath.empty())
    return;

  //-1 because we're evaluating two at a time, and -1 more because there's a null element at the end
  for(size_t i = 0; i + 2 < m_resultPath.size(); ++i)
  {
    if(i + 3 >= m_resultPath.size())
      i = i;

    Point start = m_resultPath[i];
    Point end = m_resultPath[i + 1];
    float diffX = start.x - end.x;
    float diffY = start.y - end.y;

    //If the distance is more than a diagonal, insert one in the middle
    if(diffX*diffX + diffY*diffY > 2.1f)
    {
      Point mid;
      mid.x = end.x + diffX/2;
      mid.y = end.y + diffY/2;

      InsertResult(mid, i);
      //So we evaluate i again
      i -= 2;
    }
  }
}

//Catmull rom spline for t = 0.25, 0.5, and 0.75
Point AStar::SplineSmooth1(const Point& a, const Point& b, const Point& c, const Point& d)
{
  return a*-0.0703125f + b*0.8671875f + c*0.2265666f + d*-0.0234375f;
}

Point AStar::SplineSmooth2(const Point& a, const Point& b, const Point& c, const Point& d)
{
  return a*-0.0625f + b*0.5625f + c*0.5625f + d*-0.0625f;
}

Point AStar::SplineSmooth3(const Point& a, const Point& b, const Point& c, const Point& d)
{
  return a*-0.0234375f + b*0.226563f + c*0.8671875f + d*-0.0703125f;
}

void AStar::Smooth(void)
{
  if(m_resultPath.empty())
    return;

  //-1 because we're evaluating two at a time, and -1 more because there's a null element at the end
  size_t maxIndex = m_resultPath.size() - 2;
  for(size_t i = 0; i < maxIndex; ++i)
  {
    //Start and end of path to smooth
    Point b = m_resultPath[i];
    Point c = m_resultPath[i + 1];
    //Control points for curve
    Point a, d;

    //Get ends as next and previous points unless they don't exist, in which case the control points match the start and end respectively
    if(i + 2 < maxIndex)
      d = m_resultPath[i + 2];
    else
      d = c;
    if(i == 0)
      a = b;
    else
      a = m_resultPath[i - 1];

    InsertResult(SplineSmooth1(a, b, c, d), i);
    InsertResult(SplineSmooth2(a, b, c, d), i);
    InsertResult(SplineSmooth3(a, b, c, d), i);
  }
}

void AStar::SetHeuristic(int heuristic)
{
  m_heuristic = heuristic;
  if(m_heuristic == Heuristic::Octile)
    m_costFunc = &AStar::ComputeCostOctile;
  else
    m_costFunc = &AStar::ComputeCostSwitch;
}

void AStar::ClearAltered(void)
{
  if(m_alteredList)
    TerrainNode::ClearAllAltered(m_alteredList);
}

