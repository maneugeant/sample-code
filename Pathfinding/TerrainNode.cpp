#include "TerrainNode.h"
#include <vector>

#undef max
#undef min

const unsigned TerrainNode::AllDirectionFlags = Flags::ParentN | Flags::ParentE | Flags::ParentS | Flags::ParentW;
const unsigned TerrainNode::AllListFlags = Flags::InClosedList | Flags::InOpenList;
const unsigned TerrainNode::AllNeighbors = Flags::NeighborN | Flags::NeighborNE | Flags::NeighborE |
                                            Flags::NeighborSE | Flags::NeighborS | Flags::NeighborSW |
                                            Flags::NeighborW | Flags::NeighborNW;

TerrainNode* TerrainNode::FindForward(TerrainNode* start, float cost)
{
  TerrainNode* curNode = start;
  while(curNode && curNode->GetCostTotal() < cost)
    curNode = curNode->m_next;

  return curNode;
}

TerrainNode* TerrainNode::FindBackward(TerrainNode* start, float cost)
{
  TerrainNode* curNode = start;
  if(curNode && curNode->GetCostTotal() < cost)
    curNode = nullptr;
  else
  {
    while(curNode && curNode->m_prev && curNode->GetCostTotal() > cost)
      curNode = curNode->m_prev;
    if(curNode->GetCostTotal() < cost)
      curNode = curNode->m_next;
  }

  return curNode;
}

int g_openSize = 0;

void TerrainNode::InsertOpenList(TerrainNode*& rootOpen, TerrainNode*& tailOpen, TerrainNode* toAdd)
{
  //Node to insert in front of
  TerrainNode* curNode;
  //Use ints to save a little time on the computation, approximation is fine
  float myCost = toAdd->GetCostTotal();

  //For small lists always start at head as evaluating tail adds more overhead than it's worth in that case
  if(g_openSize <= 10)
    curNode = FindForward(rootOpen, myCost);
  else
  {
    //For bigger lists it is a little faster to find the closer end and search from there
    int intCost = static_cast<int>(myCost);
    int headDiff = 0;
    int tailDiff = 0;
    headDiff = std::abs(static_cast<int>(rootOpen->GetCostTotal()) - intCost);
    tailDiff = std::abs(static_cast<int>(tailOpen->GetCostTotal()) - intCost);

    if(tailDiff < headDiff)
      curNode = FindBackward(tailOpen, myCost);
    else
      curNode = FindForward(rootOpen, myCost);
  }

  //Insert at end
  if(!curNode)
  {
    if(!rootOpen)
      rootOpen = tailOpen = toAdd;
    else
    {
      tailOpen->m_next = toAdd;
      toAdd->m_prev = tailOpen;
      tailOpen = toAdd;
    }
  }
  else
  {
    //Insert in front of curnode
    if(curNode->m_prev)
      curNode->m_prev->m_next = toAdd;
    else
      rootOpen = toAdd;
    toAdd->m_prev = curNode->m_prev;

    curNode->m_prev = toAdd;
    toAdd->m_next = curNode;
  }
  toAdd->ClearFlags(AllListFlags);
  toAdd->SetFlags(Flags::InOpenList);
  ++g_openSize;
}

void TerrainNode::RemoveOpenList(TerrainNode*& rootOpen, TerrainNode*& tailOpen, TerrainNode* toRemove)
{
  if(toRemove->m_prev)
    toRemove->m_prev->m_next = toRemove->m_next;
  else
    rootOpen = toRemove->m_next;

  if(toRemove->m_next)
    toRemove->m_next->m_prev = toRemove->m_prev;
  else
    tailOpen = toRemove->m_prev;

  toRemove->m_next = toRemove->m_prev = nullptr;
  toRemove->ClearFlags(AllListFlags);
  --g_openSize;
}

TerrainNode* TerrainNode::PopFrontOpenList(TerrainNode*& rootOpen, TerrainNode*& tailOpen)
{
  TerrainNode* front = rootOpen;
  rootOpen = rootOpen->m_next;

  if(rootOpen)
    rootOpen->m_prev = nullptr;
  else
    tailOpen = nullptr;
  front->ClearFlags(AllListFlags);
  --g_openSize;

  return front;
}

void TerrainNode::SetCost(float costToGoal, float weight)
{
  m_totalCost = m_costToOrigin + costToGoal*weight;
}

void TerrainNode::SetCost(float costToOrigin, float costToGoal, float weight)
{
  SetCostToOrigin(costToOrigin);
  m_totalCost = costToOrigin + costToGoal*weight;
}

void TerrainNode::SetCostToOrigin(float costToOrigin)
{
  m_costToOrigin = costToOrigin;
}

float TerrainNode::GetCostToGoal(float weight)
{
  return (m_totalCost - m_costToOrigin)/weight;
}

float TerrainNode::GetCostToOrigin(void)
{
  return m_costToOrigin;
}

float TerrainNode::GetCostTotal(void)
{
  return m_totalCost;
}

unsigned TerrainNode::GetUnsignedCost(void)
{
  return static_cast<unsigned>(m_totalCost);
}

void TerrainNode::SetClosed(bool value)
{
  ClearFlags(AllListFlags);
  SetFlags(Flags::InClosedList);
}

bool TerrainNode::GetClosed(void)
{
  return (m_flags & Flags::InClosedList) != 0;
}

void TerrainNode::SetIsWall(bool value)
{
  if(value)
    SetFlags(Flags::IsWall);
  else
    ClearFlags(Flags::IsWall);
}

bool TerrainNode::GetIsWall(void)
{
  return (m_flags & Flags::IsWall) != 0;
}

void TerrainNode::SetFlags(unsigned flags)
{
  m_flags |= flags;
}

void TerrainNode::ClearFlags(unsigned flags)
{
  m_flags &= ~flags;
}

void TerrainNode::AddToAltered(TerrainNode*& rootAltered, TerrainNode* toAdd)
{
  if(toAdd->m_nextAltered || (toAdd->m_flags & Flags::Altered))
    return;

  toAdd->SetFlags(Flags::Altered);
  toAdd->m_nextAltered = rootAltered;
  rootAltered = toAdd;
}

void TerrainNode::ClearAllAltered(TerrainNode*& rootAltered)
{
  while(rootAltered)
  {
    TerrainNode* next = rootAltered->m_nextAltered;
    rootAltered->Clear();
    rootAltered = next;
  }
}

void TerrainNode::Clear(void)
{
  m_costToOrigin = m_totalCost = 0.0f;
  m_next = m_prev = m_nextAltered = nullptr;
  //Clear everything except the approach direction and if it's a wall
  ClearFlags(~Flags::IsWall);
  m_parentX = m_x;
  m_parentY = m_y;
}

unsigned TerrainNode::GetFlags(void)
{
  return m_flags;
}

void TerrainNode::GetPos(int& x, int& y)
{
  x = static_cast<int>(m_x);
  y = static_cast<int>(m_y);
}

void TerrainNode::SetPos(int x, int y)
{
  m_x = m_parentX = static_cast<unsigned char>(x);
  m_y = m_parentY = static_cast<unsigned char>(y);
}

TerrainNode* TerrainNode::GetNextAltered(void)
{
  return m_nextAltered;
}

bool TerrainNode::IsClear(void)
{
  return (m_flags & (~Flags::IsWall)) == 0 && !m_next && !m_prev && !m_nextAltered;
}

void ApproachDir(TerrainNode* node, unsigned& approachDirs, unsigned dir)
{
  if(node && node->GetIsWall())
    approachDirs &= ~dir;
}

//Precompute bitfield that is used as index into array of traversal functions
void TerrainNode::SetApproach(TerrainNode* n, TerrainNode* e, TerrainNode* s, TerrainNode* w, TerrainNode* ne, TerrainNode* nw, TerrainNode* se, TerrainNode* sw)
{
  unsigned approachDirs = AllNeighbors;

  if(!n || n->GetIsWall())
    approachDirs &= ~(Flags::NeighborNW | Flags::NeighborN | Flags::NeighborNE);
  if(!e || e->GetIsWall())
    approachDirs &= ~(Flags::NeighborE | Flags::NeighborNE | Flags::NeighborSE);
  if(!s || s->GetIsWall())
    approachDirs &= ~(Flags::NeighborS | Flags::NeighborSW | Flags::NeighborSE);
  if(!w || w->GetIsWall())
    approachDirs &= ~(Flags::NeighborW | Flags::NeighborNW | Flags::NeighborSW);
  if(!ne || ne->GetIsWall())
    approachDirs &= ~Flags::NeighborNE;
  if(!nw || nw->GetIsWall())
    approachDirs &= ~Flags::NeighborNW;
  if(!se || se->GetIsWall())
    approachDirs &= ~Flags::NeighborSE;
  if(!sw || sw->GetIsWall())
    approachDirs &= ~Flags::NeighborSW;

  m_traversalFunc = approachDirs;
}
