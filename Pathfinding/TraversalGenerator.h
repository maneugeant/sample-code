#pragma once

class TerrainNode;
class AStar;

void GenerateTraversalFunctions(void);
extern void (*g_traversalFuncs[256])(TerrainNode*, AStar*);

//#define GENERATING_TRAVERSAL
