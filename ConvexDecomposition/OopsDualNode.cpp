#include "OopsDualNode.h"
#include "OopsDualGraph.h"
#include "OopsModelAdjInfo.h"

namespace Oops
{
  void DualEdge::UnHookNodes(void)
  {
    m_from->RemoveEdge(*this);
    m_to->RemoveEdge(*this);
  }

  void DualNode::AddEdge(DualEdge& edge, bool checkDuplicates)
  {
    if(checkDuplicates)
      for(auto it : m_edges)
        if(it->Equal(edge))
        {
          //A duplicate was created by reasigning an edge from a collapse.
          //Meaning this edge is redundant and can be removed
          edge.m_valid = false;
          edge.UnHookNodes();
          return;
        }
    m_edges.push_back(&edge);
  }

  void DualNode::Draw(void) const
  {
    if(!m_tri)
      return;
    Vector3 from = m_tri->GetCentroid();

    SetColor(0.0f, 1.0f, 1.0f);
    for(auto edge : m_edges)
      if(edge->GetOther(this) && edge->GetOther(this)->m_tri)
        DrawVector(from, edge->GetOther(this)->m_tri->GetCentroid() - from);

    SetColor(1.0f, 0.0f, 0.0f);
    for(auto ancestor : m_ancestors)
      DrawVector(from, ancestor->m_tri->GetCentroid() - from);
  }

  void DualNode::Collapse(DualNode& other, float maxConcavity, DualGraph& graph)
  {
    for(auto ancestor : other.m_ancestors)
      m_ancestors.push_back(ancestor);
    other.m_ancestors.clear();

    other.m_isAncestor = true;
    m_ancestors.push_back(&other);

    //Since it is becoming an ancestor, it doesn't count as an edge anymore
    RemoveEdge(this, &other);

    //Point edges that where pointing at other to this
    for(auto edge : other.m_edges)
    {
      //Skip the edge that points from other to this, as it is being discarded
      if(edge->Contains(this, &other))
        continue;

      //Point edge that was pointing at other to this
      edge->GetToThis(&other) = this;
      //Add edge will remove the edge if the above assignment caused a duplicate edge
      AddEdge(*edge, true);
    }
    other.m_edges.clear();

    for(auto edge : m_edges)
      graph.UpdateEdgeCost(*edge, maxConcavity);
  }

  void DualNode::RemoveEdge(DualNode* a, DualNode* b)
  {
    for(size_t i = 0; i < m_edges.size(); ++i)
      if(m_edges[i]->Contains(a, b))
        return SwapRemove(m_edges, i);
  }

  void DualNode::RemoveEdge(DualEdge& edge)
  {
    for(size_t i = 0; i < m_edges.size(); ++i)
      if(m_edges[i] == &edge)
        return SwapRemove(m_edges, i);
  }

  bool DualNode::HasEdge(const DualEdge& edge) const
  {
    for(auto searchEdge : m_edges)
      if(searchEdge == &edge)
        return true;
    return false;
  }

  void DualNode::AddShape(std::vector<Vector3>& result) const
  {
    Vector3 a,b,c;
    m_tri->GetFacePoints(a, b, c);
    result.push_back(a);
    result.push_back(b);
    result.push_back(c);

    //This would support arbitrary heirarchies, but it will always only be 1 level deep
    for(auto ancestor : m_ancestors)
      ancestor->AddShape(result);
  }

  void DualNode::AccumulatePerimeter(std::unordered_set<Face*>& countedTris, float& perimeter) const
  {
    HalfEdge* curEdge = m_tri->m_edge;
    if(curEdge)
      countedTris.insert(curEdge->m_face);
    for(int i = 0; i < 3; ++i)
    {
      if(!curEdge)
        break;

      //If that triangle has been visited this edge has been counted, so don't double count it
      Vector3 start = *curEdge->m_vert->m_point;
      Vector3 end = curEdge->m_next ? *curEdge->m_next->m_vert->m_point : Axes::Zero;
      Vector3 mid = (start+end)*0.5f;
      SetColor(1.0f, 0.0f, 0.0f);
      if(curEdge->m_next && 
        (!curEdge->m_twin || countedTris.find(curEdge->m_twin->m_face) == countedTris.end()))
      {
        perimeter += glm::distance(*curEdge->m_vert->m_point, *curEdge->m_next->m_vert->m_point);
        SetColor(0.0f, 1.0f, 0.0f);
        DrawBox(mid, 0.1f);
      }
      else
        DrawSphere(mid, 0.1f);


      curEdge = curEdge->m_next;
    }

    for(auto ancestor : m_ancestors)
      ancestor->AccumulatePerimeter(countedTris, perimeter);
  }
}//Oops