#pragma once
#include "OopsAABB.h"
#include "OopsRaycast.h"
#include "OopsDualNode.h"

namespace Oops
{
  class DualNode;
  struct Face;
  class ModelAdjInfo;
  struct HalfEdge;

  struct EdgeShapeCost
  {
    void Zero(void) { m_concavity = m_perimeter = m_surfaceArea = 0.0f; }
    float m_concavity;
    float m_perimeter;
    float m_surfaceArea;
    float m_total;
  };

  class DualGraph
  {
  public:
    friend class ConvexDecomposer;
    friend class DualNode;

    void Construct(ModelAdjInfo& adjInfo, float& maxConcavity);

    void GetEdgeShapeCost(const DualEdge& edge, float maxConcavity, EdgeShapeCost& result);
    void EdgeCollapse(DualEdge& edge, float maxConcavity);
    void GetRemainingNodes(std::vector<DualNode*>& results);

    float GetModelSize(void) { return m_modelSize; }

    void Draw(void);
    void DrawEdgeShape(const DualEdge& edge);
    void DrawCluster(const DualNode& cluster);
    void DrawClusterConcavity(const DualNode& cluster);

  private:
    void Allocate(ModelAdjInfo& adjInfo);
    DualNode& GetNode(Face& tri);
    //Returns if edge is null
    bool AddNeighbor(HalfEdge* edge, DualNode& original);

    bool IsDuplicateEdge(const DualEdge& edge);
    void StoreEdgeShape(const DualEdge& edge);
    float GetEdgeShapePerimiterLength(const DualEdge& edge);
    void UpdateEdgeCost(DualEdge& edge, float maxConcavity);

    bool CheckDuplicateEdges(void);
    bool ValidateNodeEdges(void);

    void StoreModelSize(ModelAdjInfo& adjInfo);

    std::vector<DualNode> m_nodes;
    std::vector<DualEdge> m_edges;
    //Temporary storage containers for an edge shape that is being evaluated
    std::vector<Vector3> m_edgeShape;
    std::vector<Vector3> m_edgeHull;
    //Used when calculating 3d perimeter to avoid counting the same edge twice
    std::unordered_set<Face*> m_perimeterEdges;
    //Used during construction
    std::unordered_map<Face*, DualNode*> m_triToNode;

    Raycaster m_raycaster;
    Hull m_huller;
    //Length of diagonal of aabb of original model. Used to ensure raycasting from outside in during concavity test
    float m_modelSize;
  };
}