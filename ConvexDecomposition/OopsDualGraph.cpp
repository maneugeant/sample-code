#include "OopsDualGraph.h"
#include "OopsModelAdjInfo.h"

//For debugging
#include "OopsSpace.h"

namespace Oops
{
  void DualGraph::Construct(ModelAdjInfo& adjInfo, float& maxConcavity)
  {
    Allocate(adjInfo);
    m_nodes.clear();
    m_edges.clear();
    m_triToNode.clear();

    for(auto& tri : adjInfo.m_faces)
    {
      DualNode& curNode = GetNode(tri);
      //If all edges have already been assigned, we can skip this one
      if(curNode.NoOfEdges() == 3)
        continue;

      //This assumes face doesn't have more than 3 sides.
      //Pointers are safe because AddNeighbor will return false on null
      HalfEdge* curEdge = tri.m_edge;
      for(int i = 0; i < 3; ++i)
      {
        if(!AddNeighbor(curEdge, curNode))
          break;
        curEdge = curEdge->m_next;
      }
    }

    StoreModelSize(adjInfo);
    for(auto& edge : m_edges)
      UpdateEdgeCost(edge, maxConcavity);
  }

  void DualGraph::StoreModelSize(ModelAdjInfo& adjInfo)
  {
    m_modelSize = 0.0f;
    if(adjInfo.m_verts.empty())
      return;

    AABB aabb;
    aabb.Initialize(*adjInfo.m_verts.front().m_point);

    for(auto& vert : adjInfo.m_verts)
      aabb.Add(*vert.m_point);

    m_modelSize = glm::length(aabb.GetDiagonal());
  }

  bool DualGraph::AddNeighbor(HalfEdge* edge, DualNode& original)
  {
    if(!edge)
      return false;
    //True because other edges still may be valid
    if(!edge->m_twin || !edge->m_twin->m_face)
      return true;

    DualNode& neighbor = GetNode(*edge->m_twin->m_face);
    DualEdge neighborEdge(&original, &neighbor);
    if(IsDuplicateEdge(neighborEdge))
      return true;
    m_edges.push_back(neighborEdge);
    DualEdge& newEdge = m_edges.back();

    original.AddEdge(newEdge, true);
    neighbor.AddEdge(newEdge, true);
    return true;
  }

  DualNode& DualGraph::GetNode(Face& tri)
  {
    size_t oldSize = m_triToNode.size();
    DualNode*& foundNode = m_triToNode[&tri];
    //New node was inserted, assign it to a new node
    if(oldSize != m_triToNode.size())
    {
      m_nodes.push_back(DualNode(&tri));
      foundNode = &m_nodes.back();
    }
    return *foundNode;
  }

  void DualGraph::Draw(void)
  {
    //This will overdraw, but it represents the structure better
    for(auto& node : m_nodes)
      node.Draw();

    /*SetColor(0.0f, 1.0f, 0.0f);
    for(size_t i = 0; i + 2 < m_edgeShape.size(); i += 3)
      DrawTriangle(&m_edgeShape[i]);

    SetColor(1.0f, 1.0f, 0.0f);
    for(size_t i = 0; i < m_edgeShape.size(); ++i)
      for(size_t j = i + 1; j < m_edgeShape.size(); ++j)
        DrawLine(m_edgeShape[i], m_edgeShape[j]);*/

    /*SetColor(0.0f, 0.0f, 1.0f);
    for(size_t i = 0; i + 2 < m_edgeShapeHull.size(); i += 3)
      DrawTriangle(&m_edgeShapeHull[i]);*/
  }

  void DualGraph::DrawEdgeShape(const DualEdge& edge)
  {
    StoreEdgeShape(edge);
    SetColor(0.0f, 1.0f, 0.0f);
    for(size_t i = 0; i + 2 < m_edgeShape.size(); i += 3)
      DrawTriangle(&m_edgeShape[i]);
  }

  void DualGraph::DrawCluster(const DualNode& cluster)
  {
    m_edgeShape.clear();
    cluster.AddShape(m_edgeShape);
    for(size_t i = 0; i + 2 < m_edgeShape.size(); i += 3)
      DrawTriangle(&m_edgeShape[i]);
  }

  void DualGraph::DrawClusterConcavity(const DualNode& cluster)
  {
    //This adds the shape to m_edgeShape, so I don't need to do that here
    DrawCluster(cluster);

    size_t mostConcave = 0;
    float highestConcavity = 0.0f;
    Vector3 bestStart, bestEnd, bestHit;

    //Iterate over each triangle
    for(size_t i = 0; i + 2 < m_edgeShape.size(); i += 3)
    {
      const Vector3& triA = m_edgeShape[i];
      const Vector3& triB = m_edgeShape[i + 1];
      const Vector3& triC = m_edgeShape[i + 2];

      //Contribution to area
      Vector3 triNormal = SafeNormalize(CCWTriangleNormal(triA, triB, triC));
      if(VectorEqual(triNormal, Axes::Zero))
        continue;

      //To test concavity, shoot a ray from outside the shape to the triangle at the convex hull.
      //Concavity is distance of this hit from triangle surface projected on its normal
      Vector3 triCenter = (triA + triB + triC)*(1.0f/3.0f);
      //Centroid of triangle minus a bit to account for numerical error
      Vector3 rayEnd = triCenter - triNormal;
      Vector3 rayStart = rayEnd + triNormal*m_modelSize;

      HitInfo rayHit;
      m_raycaster.LineCast(rayStart, rayEnd, &m_edgeShape, rayHit);

      //Difference in distance of the original triangle and the cast hit projected onto the ray
      float concavity = abs(glm::dot(rayHit.m_point, triNormal) - glm::dot(triCenter, triNormal));
      if(concavity > highestConcavity)
      {
        highestConcavity = concavity;
        mostConcave = i;
        bestStart = rayStart;
        bestEnd = rayEnd;
        bestHit = rayHit.m_point;
      }
    }

    SetColor(0.0f, 1.0f, 1.0f);
    DrawLine(bestStart, bestEnd);
    DrawBox(bestHit, 0.3f);
    unsigned supportIndex = CollisionDetection::GetSupportPoint(bestStart - bestEnd, m_edgeShape);
    DrawSphere(m_edgeShape[supportIndex], 0.3f);
  }

  void DualGraph::Allocate(ModelAdjInfo& adjInfo)
  {
    size_t tris = adjInfo.m_faces.size();
    m_nodes.reserve(tris);
    //This could be an overshot, but undershots are unacceptable as they will lead to dangling pointers
    m_edges.reserve(tris*3);
  }

  void DualGraph::EdgeCollapse(DualEdge& edge, float maxConcavity)
  {
    //This check doesn't change the results, but it will cause less copies and allocations
    if(edge.m_from->NoOfAncestors() > edge.m_to->NoOfAncestors())
      edge.m_from->Collapse(*edge.m_to, maxConcavity, *this);
    else
      edge.m_to->Collapse(*edge.m_from, maxConcavity, *this);
    //Since it has been collapsed, it is no longer a candidate edge
    edge.m_valid = false;
  }

  bool DualGraph::CheckDuplicateEdges(void)
  {
    for(size_t i = 0; i < m_edges.size(); ++i)
      if(m_edges[i].m_valid)
        for(size_t j = i + 1; j < m_edges.size(); ++j)
          if(m_edges[j].m_valid && m_edges[i].Equal(m_edges[j]))
            return true;
    return false;
  }

  bool DualGraph::ValidateNodeEdges(void)
  {
    for(auto& edge : m_edges)
      if(edge.m_valid)
        if(!edge.m_from->HasEdge(edge) || !edge.m_to->HasEdge(edge))
          return false;
    return true;
  }

  bool DualGraph::IsDuplicateEdge(const DualEdge& edge)
  {
    for(auto& searchEdge : m_edges)
      if(searchEdge.Equal(edge))
        return true;
    return false;
  }

  void DualGraph::GetEdgeShapeCost(const DualEdge& edge, float maxConcavity, EdgeShapeCost& result)
  {
    StoreEdgeShape(edge);
    result.Zero();

    //Iterate over each triangle
    for(size_t i = 0; i + 2 < m_edgeShape.size(); i += 3)
    {
      const Vector3& triA = m_edgeShape[i];
      const Vector3& triB = m_edgeShape[i + 1];
      const Vector3& triC = m_edgeShape[i + 2];

      //Contribution to area
      Vector3 triNormal = CCWTriangleNormal(triA, triB, triC);
      float normalLength = glm::length(triNormal);
      //Area of parallelogram is length of cross product, so area of triangle is half that
      result.m_surfaceArea += normalLength*0.5f;

      //Contribution to concavity
      //If this happens, I don't really know which way to raycast for concavity, but it shouldn't happen anyway
      if(normalLength < EPSILON)
        continue;

      //To test concavity, shoot a ray from outside the shape to the triangle at the convex hull.
      //Concavity is distance of this hit from triangle surface projected on its normal
      triNormal *= 1.0f/normalLength;
      Vector3 triCenter = (triA + triB + triC)*(1.0f/3.0f);
      //Centroid of triangle minus a bit to account for numerical error
      Vector3 rayEnd = triCenter - triNormal;
      Vector3 rayStart = rayEnd + triNormal*m_modelSize;

      HitInfo rayHit;
      m_raycaster.LineCast(rayStart, rayEnd, &m_edgeShape, rayHit);
      //This shouldn't happen, and would indicate a bug in my raycast
      if(!rayHit.m_valid)
        continue;

      float concavity = glm::distance(rayHit.m_point, triCenter);
      if(concavity > result.m_concavity)
        result.m_concavity = concavity;
    }

    //Add flat concavity measure
    //Divide by two since convex hull will make the underside of this flat shape, which we don't want to count
    float hullArea = CalculateSurfaceArea(m_edgeHull)*0.5f;
    float flatConcavity = sqrt(fabs(hullArea - result.m_surfaceArea));
    result.m_concavity += flatConcavity;

    result.m_perimeter = GetEdgeShapePerimiterLength(edge);
    result.m_concavity = SafeDivide(result.m_concavity, m_modelSize);

    //This equation from the paper
    //Parameter controlling contribution of aspect ratio to collapse choice
    float alpha = SafeDivide(maxConcavity, 10.0f*m_modelSize);
    result.m_total = result.m_concavity + 
      alpha*SafeDivide(result.m_perimeter*result.m_perimeter, 4.0f*glm::pi<float>()*result.m_surfaceArea);
  }

  void DualGraph::UpdateEdgeCost(DualEdge& edge, float maxConcavity)
  {
    EdgeShapeCost cost;
    GetEdgeShapeCost(edge, maxConcavity, cost);
    edge.m_cost = cost.m_total;
    edge.m_concavity = cost.m_concavity;
  }

  float DualGraph::GetEdgeShapePerimiterLength(const DualEdge& edge)
  {
    m_perimeterEdges.clear();
    float result = 0.0f;

    edge.m_from->AccumulatePerimeter(m_perimeterEdges, result);
    edge.m_to->AccumulatePerimeter(m_perimeterEdges, result);
    return result;
  }

  void DualGraph::StoreEdgeShape(const DualEdge& edge)
  {
    m_edgeShape.clear();
    edge.m_from->AddShape(m_edgeShape);
    edge.m_to->AddShape(m_edgeShape);
    //Enough iterations to make a complete hull
    m_huller.MakeConvexHull(m_edgeShape, m_edgeHull, 2000);
  }

  void DualGraph::GetRemainingNodes(std::vector<DualNode*>& results)
  {
    results.clear();
    for(auto& node : m_nodes)
      if(!node.IsAncestor())
        results.push_back(&node);
  }
}//Oops