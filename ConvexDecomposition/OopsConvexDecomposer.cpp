#include "OopsConvexDecomposer.h"

namespace Oops
{
  int ConvexDecomposer::GetEdgeIndex(unsigned desiredIndex)
  {
    unsigned curEdge = 0;
    for(size_t i = 0; i < m_graph.m_edges.size(); ++i)
      if(m_graph.m_edges[i].m_valid)
      {
        if(desiredIndex == curEdge)
          return static_cast<int>(i);
        ++curEdge;
      }
    return -1;
  }

  int ConvexDecomposer::GetClusterIndex(unsigned desiredIndex)
  {
    unsigned curCluster = 0;
    for(size_t i = 0; i < m_graph.m_nodes.size(); ++i)
      if(!m_graph.m_nodes[i].IsAncestor())
      {
        if(curCluster == desiredIndex)
          return static_cast<int>(i);
        ++curCluster;
      }
    return -1;
  }

  void ConvexDecomposer::Draw(void)
  {
    m_graph.Draw();
  }

  void ConvexDecomposer::DrawCluster(unsigned clusterIndex, bool drawConcavity)
  {
    int index = GetClusterIndex(clusterIndex);
    if(index == -1)
      return;
    if(drawConcavity)
      m_graph.DrawClusterConcavity(m_graph.m_nodes[index]);
    else
      m_graph.DrawCluster(m_graph.m_nodes[index]);
  }

  void ConvexDecomposer::DrawAllClusters(bool drawConcavity)
  {
    for(size_t i = 0; i < m_graph.m_nodes.size(); ++i)
      if(!m_graph.m_nodes[i].IsAncestor())
      {
        if(drawConcavity)
          m_graph.DrawClusterConcavity(m_graph.m_nodes[i]);
        else
          m_graph.DrawCluster(m_graph.m_nodes[i]);
      }
  }

  size_t ConvexDecomposer::NoOfClusters(void)
  {
    unsigned result = 0;
    for(size_t i = 0; i < m_graph.m_nodes.size(); ++i)
      if(!m_graph.m_nodes[i].IsAncestor())
        ++result;
    return result;
  }

  void ConvexDecomposer::DrawEdgeCost(unsigned edgeIndex)
  {
    int index = GetEdgeIndex(edgeIndex);
    if(index == -1)
      return;
    DualEdge& edge = m_graph.m_edges[index];
    m_graph.DrawEdgeShape(edge);
    EdgeShapeCost cost;
    m_graph.GetEdgeShapeCost(edge, 0.1f, cost);
  }

  void ConvexDecomposer::ConvexDecompose(ModelAdjInfo& adjInfo, std::vector<std::vector<Vector3>>& results, float maxConcavity)
  {
    m_graph.Construct(adjInfo, maxConcavity);

    DualEdge* curEdge = GetBestEdge(maxConcavity);
    while(curEdge)
    {
      m_graph.EdgeCollapse(*curEdge, maxConcavity);
      curEdge = GetBestEdge(maxConcavity);
    }

    StoreResults(results);
  }

  void ConvexDecomposer::StoreResults(std::vector<std::vector<Vector3>>& results)
  {
    results.clear();
    std::vector<DualNode*> partitions;
    m_graph.GetRemainingNodes(partitions);

    results.resize(partitions.size());
    for(size_t i = 0; i < partitions.size(); ++i)
      partitions[i]->AddShape(results[i]);
  }

  DualEdge* ConvexDecomposer::GetBestEdge(float maxConcavity)
  {
    DualEdge* bestEdge = nullptr;

    //This loop could be optimized by storing best when edges are updated, but this loop isn't a bottlneck
    //If edge isn't already collapsed and
    //If the concavity caused by this collapse would be acceptable and it is cheaper than the previous, store it
    for(auto& edge : m_graph.m_edges)
      if(edge.m_valid && edge.m_concavity <= maxConcavity && (!bestEdge || edge.m_cost < bestEdge->m_cost))
        bestEdge = &edge;

    //Will return null once the shape is as decomposed as is possible with the given concavity restriction
    return bestEdge;
  }
}//Oops