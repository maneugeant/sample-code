#pragma once

namespace Oops
{
  class DualNode;
  struct Face;
  class ModelAdjInfo;
  struct HalfEdge;
  class DualGraph;

  struct DualEdge
  {
    DualEdge(void): m_from(nullptr), m_to(nullptr), m_valid(true) {}
    DualEdge(DualNode* from, DualNode* to): m_from(from), m_to(to), m_valid(true) {}

    DualNode* GetOther(const DualNode* original) { if(m_from == original) return m_to; return m_from; }
    DualNode*& GetToThis(const DualNode* original) { if(m_from == original) return m_from; return m_to; }
    bool Contains(const DualNode* node) const { return m_to == node || m_from == node; }
    bool Contains(const DualNode* a, const DualNode* b) const { return Contains(a) && Contains(b); }
    bool Equal(const DualEdge& other) const { return Contains(other.m_from, other.m_to); }
    void UnHookNodes(void);

    DualNode* m_from;
    DualNode* m_to;
    float m_cost;
    float m_concavity;
    bool m_valid;
  };

  class DualNode
  {
  public:
    DualNode(Face* tri = nullptr): m_tri(tri), m_isAncestor(false) {}

    void Draw(void) const;

    //Collapse other into this.
    void Collapse(DualNode& other, float maxConcavity, DualGraph& graph);
    void AddEdge(DualEdge& edge, bool checkDuplicates);
    //Remove the edge going from a to b or b to a
    void RemoveEdge(DualNode* a, DualNode* b);
    void RemoveEdge(DualEdge& edge);
    bool HasEdge(const DualEdge& edge) const;
    bool IsAncestor(void) const { return m_isAncestor; }

    void AddShape(std::vector<Vector3>& result) const;
    void AccumulatePerimeter(std::unordered_set<Face*>& countedTris, float& perimeter) const;

    size_t NoOfEdges(void) { return m_edges.size(); }
    size_t NoOfAncestors(void) { return m_ancestors.size(); }

  private:
    Face* m_tri;
    std::vector<DualEdge*> m_edges;
    std::vector<DualNode*> m_ancestors;
    bool m_isAncestor;
  };
}//Oops