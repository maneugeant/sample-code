#pragma once
#include "OopsDualGraph.h"

namespace Oops
{
  class ModelAdjInfo;

  class ConvexDecomposer
  {
  public:
    void ConvexDecompose(ModelAdjInfo& adjInfo, std::vector<std::vector<Vector3>>& results, float maxConcavity);
    void Draw(void);
    void DrawEdgeCost(unsigned edgeIndex);
    //Draw one of the resulting convex partitions
    void DrawCluster(unsigned clusterIndex, bool drawConcavity);
    void DrawAllClusters(bool drawConcavity);
    size_t NoOfClusters(void);

  private:
    //Get index in m_edges of graph excluding collapsed edges. -1 if out of bounds
    int GetEdgeIndex(unsigned desiredIndex);
    //Same as get edge except for clusters
    int GetClusterIndex(unsigned desiredIndex);
    DualEdge* GetBestEdge(float maxConcavity);
    void StoreResults(std::vector<std::vector<Vector3>>& results);

    DualGraph m_graph;
  };
}