#pragma once
#include "SyxSVector3.h"

namespace Syx
{
  struct SQuat;
  struct Matrix3;

  struct SMatrix3
  {
    SMatrix3(void) {}
    //First letter is row, second is column
    SMatrix3(float aa, float ab, float ac, float ba, float bb, float bc, float ca, float cb, float cc);
    SMatrix3(const SVector3& bx, const SVector3& by, const SVector3& bz);
    SMatrix3(const SVector3& diag);

    SMatrix3 operator*(const SMatrix3& rhs) const;
    SMatrix3& operator*=(const SMatrix3& rhs);
    SVector3 operator*(const SVector3& rhs) const;
    SMatrix3 operator+(const SMatrix3& rhs) const;
    SMatrix3& operator+=(const SMatrix3& rhs);
    SMatrix3 operator-(const SMatrix3& rhs) const;
    SMatrix3& operator-=(const SMatrix3& rhs);

    SQuat ToQuat(void);

    SMatrix3 Transposed(void) const;
    void Transpose(void);
    //Not faster when using SIMD versions, so if possible, cache the transpose and re-use it
    SMatrix3 TransposedMultiply(const SMatrix3& rhs) const;
    SVector3 TransposedMultiply(const SVector3& rhs) const;

    SMatrix3 Inverse(void) const;
    SMatrix3 Inverse(const SVector3& det) const;
    SVector3 Determinant(void) const;

    static const SMatrix3 Identity;
    static const SMatrix3 Zero;

    //Basis vectors, first, second and third columns respectively
    SVector3 m_bx;
    SVector3 m_by;
    SVector3 m_bz;
  };

  SMatrix3 operator*(const SVector3& lhs, const SMatrix3& rhs);
  SMatrix3 ToSMatrix3(const Matrix3& mat);
}