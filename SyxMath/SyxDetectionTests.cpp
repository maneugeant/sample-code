#include "Precompile.h"
#include "SyxDetectionTests.h"
#include "SyxTestHelpers.h"
#include "SyxModel.h"
#include "SyxTransform.h"
#include "SyxSSimplex.h"
#include "SyxGeometricQueries.h"

namespace Syx
{
  extern bool TEST_FAILED;
  bool TestAllDetection(void)
  {
    TestModel();
    if(TEST_FAILED)
      return TEST_FAILED;
    TestTransform();
    return TEST_FAILED;
  }

  bool TestModel(void)
  {
    //Throw a bunch of random points at it and make sure it always picks the right support
    std::vector<Vector3> modelPoints;
    std::vector<SVector3, AlignmentAllocator<SVector3>> sPoints;
    for(int i = 0; i < 1000; ++i)
    {
      Vector3 v = VecRand(-10000, 10000);
      modelPoints.push_back(v);
      sPoints.push_back(SVector3(v.x, v.y, v.z, 0.0f));
    }

    Model model(sPoints);
    for(size_t i = 0; i < sPoints.size(); ++i)
    {
      SVector3 sSupport = model.GetSupport(sPoints[i]);
      Vector3 support;
      sSupport.Store(support);

      //Validate support point by hand by brute forcing for one
      float bestDot = support.Dot(modelPoints[i]);
      for(const Vector3& testPoint : modelPoints)
        if(testPoint.Dot(modelPoints[i]) > bestDot + SYX_EPSILON)
          FailTest();
    }
    return TEST_FAILED;
  }

  bool TestTransform(void)
  {
    Transform parent;
    parent.m_pos = SVector3(1.0f, 2.0f, 3.0f);
    parent.m_rot = SQuat::AxisAngle(SVector3(1.0f, 4.0f, -1.0f).Normalized(), 1.0f);
    parent.m_scale = SVector3(0.5f, 3.0f, 0.9f);

    Transform child;
    child.m_pos = SVector3(4.0f, -2.0f, -1.0f);
    child.m_rot = SQuat::AxisAngle(SVector3(-1.0f, -1.0f, -1.0f).Normalized(), 0.3f);
    child.m_scale = SVector3(2.5f, 1.0f, 1.9f);

    Transformer childToWorlder = child.GetModelToWorld();
    Transformer worldToChilder = child.GetWorldToModel();
    Transformer childToParentToWorlder = parent.GetModelToWorld(child);
    Transformer worldToParentToChilder = parent.GetWorldToModel(child);

    SVector3 testPoint(0.5f, 4.0f, 2.0f);
    //Verified by hand with a calculator
    Vector3 childToWorld(5.36f, 2.40f, 2.28f);
    Vector3 worldToChild(-1.51f, 4.66f, 2.42f);
    Vector3 childToParentToWorld(6.27f, 7.96f, 2.69f);
    Vector3 worldToParentToChild(-1.50f, 1.95f, 0.16f);
    //Really high epsilon since I didn't use much precision for the calculations
    float epsilon = 0.1f;

    CheckResult(child.ModelToWorld(testPoint), childToWorld, epsilon);
    CheckResult(child.WorldToModel(testPoint), worldToChild, epsilon);
    CheckResult(child.WorldToModel(child.ModelToWorld(testPoint)), Vector3(testPoint[0], testPoint[1], testPoint[2]), epsilon);
    CheckResult(childToWorlder.TransformPoint(testPoint), childToWorld, epsilon);
    CheckResult(worldToChilder.TransformPoint(testPoint), worldToChild, epsilon);
    CheckResult(worldToChilder.TransformPoint(childToWorlder.TransformPoint(testPoint)), Vector3(testPoint[0], testPoint[1], testPoint[2]), epsilon);
    CheckResult(childToParentToWorlder.TransformPoint(testPoint), childToParentToWorld, epsilon);
    CheckResult(worldToParentToChilder.TransformPoint(testPoint), worldToParentToChild, epsilon);
    return TEST_FAILED;
  }

  void PerformLineTest(const SVector3& a, const SVector3& b, SSimplex& simplex, size_t expectedSize)
  {
    simplex.Initialize();
    simplex.Add(SupportPoint(a), false);
    simplex.Add(SupportPoint(b), false);

    SVector3 simplexResult = simplex.Solve();
    Vector3 baseResult = -ClosestOnLineSegment(ToVector3(a), ToVector3(b), Vector3::Zero);
    CheckResult(simplexResult, baseResult);
    CheckResult(simplex.Size(), expectedSize);
  }

  void TestSimplexLine(void)
  {
    SSimplex simplex;
    //Simplex doesn't check for behind a because it shouldn't happen in GJK, so don't test for that
    SVector3 a, b, simplexResult;

    //Between a and b
    a = SVector3(-1.0, -1.0f, -1.0f);
    b = SVector3(2.0f, 1.0f, 2.0f);
    PerformLineTest(a, b, simplex, 2);

    //In front of b
    b = SVector3(1.0f, 1.0f, 1.0f);
    a = SVector3(2.0f, 2.0f, 2.0f);
    PerformLineTest(a, b, simplex, 1);
    CheckResult(simplex.Get(SupportID::A), b);
  }

  void PerformTriangleTest(const SVector3& a, const SVector3& b, const SVector3& c, SSimplex& simplex, size_t expectedSize)
  {
    simplex.Initialize();
    simplex.Add(SupportPoint(a), false);
    simplex.Add(SupportPoint(b), false);
    simplex.Add(SupportPoint(c), false);

    SVector3 simplexResult = simplex.Solve();
    Vector3 baseResult = -ClosestOnTriangle(ToVector3(a), ToVector3(b), ToVector3(c), Vector3::Zero);
    CheckResult(simplexResult.Normalized(), baseResult.Normalized());
    CheckResult(simplex.Size(), expectedSize);
  }

  void TestSimplexTriangle(void)
  {
    //Simplex doesn't check for behind ab, so don't test for that
    SVector3 a, b, c;
    SSimplex simplex;

    //in front of c
    c = SVector3(1.0f, 1.0f, 0.0f);
    b = SVector3(2.0f, 1.0f, 0.0f);
    a = SVector3(1.0f, 2.0f, 0.0f);
    PerformTriangleTest(a, b, c, simplex, 1);
    CheckResult(simplex.Get(0), c);

    //in front of bc
    a = SVector3(0.5f, -2.0f, 0.0f);
    b = SVector3(1.0f, -1.0f, 0.0f);
    c = SVector3(-1.0f, -1.0f, 0.0f);
    PerformTriangleTest(a, b, c, simplex, 2);
    CheckResult(simplex.Get(SupportID::A), b);
    CheckResult(simplex.Get(SupportID::B), c);

    //in front of ac
    b = SVector3(0.5f, -2.0f, 0.0f);
    a = SVector3(1.0f, -1.0f, 0.0f);
    c = SVector3(-1.0f, -1.0f, 0.0f);
    PerformTriangleTest(a, b, c, simplex, 2);
    CheckResult(simplex.Get(SupportID::A), a);
    CheckResult(simplex.Get(SupportID::B), c);

    //within triangle above
    a = SVector3(-1.0f, -1.0f, -1.0f);
    b = SVector3(1.0f, -1.0f, -1.0f);
    c = SVector3(0.0f, 1.0f, -1.0f);
    PerformTriangleTest(a, b, c, simplex, 3);
    CheckResult(simplex.Solve().Dot(-a)[0] > 0.0f);

    //within triangle below
    a = SVector3(-1.0f, -1.0f, 1.0f);
    b = SVector3(1.0f, -1.0f, 1.0f);
    c = SVector3(0.0f, 1.0f, 1.0f);
    PerformTriangleTest(a, b, c, simplex, 3);
    CheckResult(simplex.Solve().Dot(-a)[0] > 0.0f);
  }

  void PerformTetrahedronTest(const SVector3& a, const SVector3& b, const SVector3& c, const SVector3& d, SSimplex& simplex, size_t expectedSize)
  {
    //Verify winding of input tetrahedron, otherwise it's an unfair test
    SVector3 mid = (a + b + c + d)*SVector3(0.25f);
    SVector3 abc = TriangleNormal(a, b, c);
    SVector3 bdc = TriangleNormal(b, d, c);
    SVector3 dac = TriangleNormal(d, a, c);
    SVector3 adb = TriangleNormal(a, d, b);
    SVector3 abcDir = (mid - a).Dot(abc);
    SVector3 bdcDir = (mid - b).Dot(bdc);
    SVector3 dacDir = (mid - d).Dot(dac);
    SVector3 adbDir = (mid - a).Dot(adb);
    SyxAssertError(abcDir[0] < 0.0f && bdcDir[0] < 0.0f && dacDir[0] < 0.0f && adbDir[0] < 0.0f,
      "Improperly wound input tetrahedron.");

    simplex.Initialize();
    simplex.Add(SupportPoint(a), false);
    simplex.Add(SupportPoint(b), false);
    simplex.Add(SupportPoint(c), false);
    simplex.Add(SupportPoint(d), false);

    SVector3 simplexResult = simplex.Solve();
    Vector3 baseResult = -ClosestOnTetrahedron(ToVector3(a), ToVector3(b), ToVector3(c), ToVector3(d), Vector3::Zero);
    CheckResult(simplexResult.SafeNormalized(), baseResult.SafeNormalized());
    CheckResult(simplex.Size(), expectedSize);
  }

  void TestSimplexTetrahedron(void)
  {
    //Simplex doesn't check for in front of abc so don't test for that
    SSimplex simplex;
    SVector3 a, b, c, d;

    //In front of dba
    a = SVector3(1.0f, 0.0f, -2.0f);
    b = SVector3(-1.0f, 0.0f, -2.0f);
    c = SVector3(-0.2f, 0.3f, -2.0f);
    d = SVector3(0.0f, 0.5f, 1.0f);
    PerformTetrahedronTest(a, b, c, d, simplex, 3);
    CheckResult(simplex.Get(0), a);
    CheckResult(simplex.Get(1), b);
    CheckResult(simplex.Get(2), d);

    //In front of dcb
    a = SVector3(1.28f, -0.37f, -1.16f);
    b = SVector3(-0.72f, -0.42f, -1.08f);
    c = SVector3(0.147f, -0.072f, -1.08f);
    d = SVector3(0.347f, 0.128f, 1.922f);
    PerformTetrahedronTest(a, b, c, d, simplex, 3);
    CheckResult(simplex.Get(0), b);
    CheckResult(simplex.Get(1), c);
    CheckResult(simplex.Get(2), d);

    //In front of dac
    a = SVector3(0.68f, -0.6f, -1.85f);
    b = SVector3(-1.3f, 0.0f, 0.0f);
    c = SVector3(-0.45f, -0.3f, -1.77f);
    d = SVector3(-0.25f, -0.11f, 1.23f);
    PerformTetrahedronTest(a, b, c, d, simplex, 3);
    CheckResult(simplex.Get(0), a);
    CheckResult(simplex.Get(1), c);
    CheckResult(simplex.Get(2), d);

    //In front of da
    a = SVector3(-0.188f, -0.33f, -1.163f);
    b = SVector3(-2.188f, -0.381f, -1.145f);
    c = SVector3(-1.322f, -0.03f, -1.018f);
    d = SVector3(-1.122f, 0.17f, 1.922f);
    PerformTetrahedronTest(a, b, c, d, simplex, 2);
    CheckResult(simplex.Get(0), a);
    CheckResult(simplex.Get(1), d);

    //In front of db
    a = SVector3(1.935f, -0.276f, -1.163f);
    b = SVector3(-0.163f, -0.145f, -1.215f);
    c = SVector3(0.8f, 0.024f, -1.02f);
    d = SVector3(1.0f, 0.224f, 1.922f);
    PerformTetrahedronTest(a, b, c, d, simplex, 2);
    CheckResult(simplex.Get(0), b);
    CheckResult(simplex.Get(1), d);

    //In front of dc
    a = SVector3(1.051f, -0.749f, -1.163f);
    b = SVector3(-0.948f, -0.8f, -1.145f);
    c = SVector3(-0.083f, -0.449f, -1.018f);
    d = SVector3(0.117f, -0.249f, 1.922f);
    PerformTetrahedronTest(a, b, c, d, simplex, 2);
    CheckResult(simplex.Get(0), c);
    CheckResult(simplex.Get(1), d);

    //In front of d
    a = SVector3(1.092f, -0.33f, -3.571f);
    b = SVector3(-0.907f, -0.381f, -3.553f);
    c = SVector3(-0.042f, -0.03f, -3.426f);
    d = SVector3(0.158f, 0.17f, -0.486f);
    PerformTetrahedronTest(a, b, c, d, simplex, 1);
    CheckResult(simplex.Get(0), d);

    //Inside tetrahedron
    a = SVector3(1.092f, -0.16f, -0.24f);
    b = SVector3(-0.907f, -0.211f, -0.221f);
    c = SVector3(-0.042f, 0.14f, -0.094f);
    d = SVector3(0.158f, 0.34f, 2.845f);
    PerformTetrahedronTest(a, b, c, d, simplex, 4);
  }

  bool TestSimplex(void)
  {
    TEST_FAILED = false;
    TestSimplexLine();
    TestSimplexTriangle();
    TestSimplexTetrahedron();
    return TEST_FAILED;
  }
}