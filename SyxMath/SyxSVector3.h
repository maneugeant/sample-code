#pragma once
#include "SyxSIMD.h"

namespace Syx
{
  struct Vector3;

  struct SVector3
  {
    SVector3(SFloats vec): m_v(vec) {}
    SVector3(float x, float y, float z, float w = 0.0f);
    SVector3(float splat);
    SVector3(void) {}

    SFloats& operator__m128(void) { return m_v; }

    //Needs 4 floats
    void LoadAll(const float* source);
    void LoadAll(float x, float y, float z, float w = 0.0f);
    void LoadAll(const Vector3& in);
    void LoadSplat(const float* source);
    void Store(float* destination) const;
    void Store(Vector3& destination) const;

    //Expensive. Avoid use when possible
    float operator[](int index) const { SIMDAlign float store[4]; Store(store); return store[index]; }
    SVector3 operator+(const SVector3& rhs) const;
    SVector3 operator-(const SVector3& rhs) const;
    SVector3 operator-(void) const;
    SVector3 operator*(const SVector3& rhs) const;
    SVector3 operator/(const SVector3& rhs) const;
    //Logical and
    SVector3 operator&(const SVector3& rhs) const;
    SVector3 operator|(const SVector3& rhs) const;

    SVector3& operator+=(const SVector3& rhs);
    SVector3& operator-=(const SVector3& rhs);
    SVector3& operator*=(const SVector3& rhs);
    SVector3& operator/=(const SVector3& rhs);

    SVector3 operator==(const SVector3& rhs) const;
    SVector3 operator!=(const SVector3& rhs) const;
    //Returns all zeroes of anything doesn't match, and all bits on if all match
    SVector3 Equal(const SVector3& rhs, const SVector3& epsilon) const;
    SVector3 NotEqual(const SVector3& rhs, const SVector3& epsilon) const;
    void GetBasis(SVector3& resultA, SVector3& resultB) const;
    SVector3 Length(void) const;
    SVector3 Length2(void) const;
    SVector3 Distance(const SVector3& other) const;
    SVector3 Distance2(const SVector3& other) const;
    SVector3 Dot(const SVector3& other) const;
    SVector3 Dot4(const SVector3& other) const;
    SVector3 Cross(const SVector3& other) const;
    //Returns index of least significant, so x = 0, y = 1, etc.
    SVector3 LeastSignificantAxis(void) const;
    SVector3 MostSignificantAxis(void) const;
    SVector3 Normalized(void) const;
    SVector3 SafeNormalized(void) const;
    SVector3 SafeDivide(const SVector3& rhs) const;
    SVector3 Abs(void) const;
    SVector3 ProjVec(const SVector3& onto) const;
    //The scalar portion of the projection, u.v/u.u
    SVector3 ProjVecScalar(const SVector3& onto) const;
    SVector3 Reciprocal(void) const;

    static SVector3 SafeDivide(const SVector3& vec, const SVector3& div);
    static SVector3 Lerp(const SVector3& start, const SVector3& end, const SVector3& t);
    static SVector3 Abs(const SVector3& in);
    // Finds the distance to the unlimited line
    static SVector3 PointLineDistanceSQ(const SVector3& point, const SVector3& start, const SVector3& end);
    static SVector3 CCWTriangleNormal(const SVector3& a, const SVector3& b, const SVector3& c);
    static SVector3 PerpendicularLineToPoint(const SVector3& line, const SVector3& lineToPoint);
    static SVector3 BarycentricToPoint(const SVector3& a, const SVector3& b, const SVector3& c, const SVector3& ba, const SVector3& bb, const SVector3& bc);
    static SVector3 ProjVec(const SVector3& vec, const SVector3& onto);
    static SVector3 ProjVecScalar(const SVector3& vec, const SVector3& onto);
    static SVector3 PointPlaneProj(const SVector3& point, const SVector3& normal, const SVector3& onPlane);
    static SVector3 GetScalarT(const SVector3& start, const SVector3& end, const SVector3& pointOnLine);
    static SVector3 Length(const SVector3& in);
    static SVector3 Length2(const SVector3& in);
    static SVector3 Distance(const SVector3& lhs, const SVector3& rhs);
    static SVector3 Distance2(const SVector3& lhs, const SVector3& rhs);
    static SVector3 Dot(const SVector3& lhs, const SVector3& rhs);
    static SVector3 Dot4(const SVector3& lhs, const SVector3& rhs);
    static SVector3 Cross(const SVector3& lhs, const SVector3& rhs);
    static SVector3 LeastSignificantAxis(const SVector3& in);
    static SVector3 MostSignificantAxis(const SVector3& in);

    const static SVector3 UnitX;
    const static SVector3 UnitY;
    const static SVector3 UnitZ;
    const static SVector3 Zero;
    const static SVector3 Identity;
    const static SVector3 Epsilon;
    const static SVector3 BitsX;
    const static SVector3 BitsY;
    const static SVector3 BitsZ;
    const static SVector3 BitsW;
    const static SVector3 BitsAll;

    SFloats m_v;
  };

  //These aren't constructors for the sake of being explicit, as these conversions are expensive and shouldn't be taken lightly
  SVector3 ToSVector3(const Vector3& vec);
  Vector3 ToVector3(const SVector3& vec);
}
