#pragma once
#include "Precompile.h"

namespace Syx
{
  extern bool TEST_FAILED;

  inline void FailTest(void)
  {
    TEST_FAILED = true;
  }

  template <typename T>
  void CheckResult(const T& a, const T& b)
  {
    if(a != b)
      FailTest();
  }

  inline void CheckResult(float a, float b)
  {
    if(abs(a - b) > SYX_EPSILON)
      FailTest();
  }

  inline void CheckResult(const SVector3& a, const SVector3& b)
  {
    if((a == b)[0] == 0.0f)
      FailTest();
  }

  inline void CheckResult(const SVector3& lhs, float x, float y, float z, float w, float epsilon)
  {
    SIMDAlign float store[4];
    lhs.Store(store);
    if(!Vector3(store[0], store[1], store[2]).Equal(Vector3(x, y, z), epsilon) || abs(store[3]-w) > epsilon)
      FailTest();
  }

  inline void CheckResult(const SVector3& lhs, float x, float y, float z, float epsilon)
  {
    Vector3 result;
    lhs.Store(result);
    if(!result.Equal(Vector3(x, y, z), epsilon))
      FailTest();
  }

  inline void CheckResult(const SVector3& lhs, float splat, float epsilon = SYX_EPSILON)
  {
    CheckResult(lhs, splat, splat, splat, epsilon);
  }

  inline void CheckResult(const SVector3& lhs, const Vector3& rhs, float epsilon = SYX_EPSILON)
  {
    CheckResult(lhs, rhs.x, rhs.y, rhs.z, epsilon);
  }

  inline void CheckResult(const SQuat& lhs, const Quat& rhs, float epsilon = SYX_EPSILON)
  {
    CheckResult(lhs.m_v, rhs.m_v.x, rhs.m_v.y, rhs.m_v.z, rhs.m_w, epsilon);
  }

  inline void CheckResult(const SMatrix3& lhs, const Matrix3& rhs, float epsilon = SYX_EPSILON)
  {
    CheckResult(lhs.m_bx, rhs.m_bx, epsilon);
    CheckResult(lhs.m_by, rhs.m_by, epsilon);
    CheckResult(lhs.m_bz, rhs.m_bz, epsilon);
  }

  inline void CheckResult(bool result)
  {
    if(!result)
      FailTest();
  }

  inline float FloatRand(float min, float max)
  {
    return min + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (max - min)));
  }

  inline Vector3 VecRand(float min, float max)
  {
    return Vector3(FloatRand(min, max), FloatRand(min, max), FloatRand(min, max));
  }

  inline SVector3 SVecRand(float min, float max)
  {
    Vector3 v = VecRand(min, max);
    return SVector3(v.x, v.y, v.z, 0.0f);
  }
}