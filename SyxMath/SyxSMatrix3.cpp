#include "Precompile.h"
#include "SyxSMatrix3.h"
#include "SyxSVector3.h"
#include "SyxSIMD.h"
#include "SyxSQuaternion.h"
#include "SyxMatrix3.h"
#include "SyxQuaternion.h"

namespace Syx
{
  const SMatrix3 SMatrix3::Identity = SMatrix3(1.0f, 0.0f, 0.0f,
                                            0.0f, 1.0f, 0.0f,
                                            0.0f, 0.0f, 1.0f);
  const SMatrix3 SMatrix3::Zero = SMatrix3(0.0f, 0.0f, 0.0f,
                                        0.0f, 0.0f, 0.0f,
                                        0.0f, 0.0f, 0.0f);

  SMatrix3::SMatrix3(float aa, float ab, float ac,
                     float ba, float bb, float bc,
                     float ca, float cb, float cc):
                     m_bx(aa, ba, ca), m_by(ab, bb, cb), m_bz(ac, bc, cc) {}

  SMatrix3::SMatrix3(const SVector3& bx, const SVector3& by, const SVector3& bz): m_bx(bx), m_by(by), m_bz(bz) {}

  SMatrix3::SMatrix3(const SVector3& diag)
  {
    m_bx = SAnd(diag.m_v, SVector3::BitsX.m_v);
    m_by = SAnd(diag.m_v, SVector3::BitsY.m_v);
    m_bz = SAnd(diag.m_v, SVector3::BitsZ.m_v);
  }

  //Dot xyz and leave the result in [0]
  SFloats Dot3(SFloats lhs, SFloats rhs)
  {
    SFloats mul = SMultiplyAll(lhs, rhs);
    //yxy
    SFloats temp = SShuffle(mul, 1, 0, 1, 3);
    SFloats result = SAddAll(mul, temp);
    //zzx
    temp = SShuffle(mul, 2, 2, 0, 3);
    return SAddAll(result, temp);
  }

  //Produce one column of the multiplied matrix, given the column (vec) of the matrix to multiply with
  /*
  [a b c] [j] [aj+bk+cl]
  [d e f]*[k]=[dj+ek+fl]
  [g h i] [l] [gj+hk+il]*/
  SVector3 MultColumn(const SMatrix3& mat, const SVector3& vec)
  {
    SVector3 result = SMultiplyAll(mat.m_bx.m_v, SShuffle(vec.m_v, 0, 0, 0, 0));
    result = SAddAll(result.m_v, SMultiplyAll(mat.m_by.m_v, SShuffle(vec.m_v, 1, 1, 1, 1)));
    result = SAddAll(result.m_v, SMultiplyAll(mat.m_bz.m_v, SShuffle(vec.m_v, 2, 2, 2, 2)));
    return result;
  }

  /*
  [a b c] [j k l] [aj+bm+cp, ak+bn+cq, al+bo+cr]
  [d e f]*[m n o]=[dj+em+fp, dk+en+fq, dl+eo+fr]
  [g h i] [p q r] [gj+hm+ip, gk+hn+iq, gl+ho+ir]*/
  SMatrix3 SMatrix3::operator*(const SMatrix3& rhs) const
  {
    SMatrix3 result;
    result.m_bx = MultColumn(*this, rhs.m_bx);
    result.m_by = MultColumn(*this, rhs.m_by);
    result.m_bz = MultColumn(*this, rhs.m_bz);
    return result;
  }

  SMatrix3& SMatrix3::operator*=(const SMatrix3& rhs)
  {
    return *this = *this * rhs;
  }

  SVector3 SMatrix3::operator*(const SVector3& rhs) const
  {
    return MultColumn(*this, rhs);
  }

  SMatrix3 SMatrix3::operator+(const SMatrix3& rhs) const
  {
    return SMatrix3(m_bx + rhs.m_bx, m_by + rhs.m_by, m_bz + rhs.m_bz);
  }

  SMatrix3& SMatrix3::operator+=(const SMatrix3& rhs)
  {
    m_bx += rhs.m_bx;
    m_by += rhs.m_by;
    m_bz += rhs.m_bz;
    return *this;
  }

  SMatrix3 SMatrix3::operator-(const SMatrix3& rhs) const
  {
    return SMatrix3(m_bx - rhs.m_bx, m_by - rhs.m_by, m_bz - rhs.m_bz);
  }

  SMatrix3& SMatrix3::operator-=(const SMatrix3& rhs)
  {
    m_bx -= rhs.m_bx;
    m_by -= rhs.m_by;
    m_bz -= rhs.m_bz;
    return *this;
  }

  SMatrix3 SMatrix3::Transposed(void) const
  {
    SMatrix3 result;
    //[r0,?,r1,?]
    result.m_bx = SShuffle2(m_bx.m_v, m_by.m_v, 0, 3, 0, 3);
    //[r0,r1,?,?]
    result.m_bx = SShuffle(result.m_bx.m_v, 0, 2, 3, 3);
    //[r0,r1,r2,?]
    result.m_bx = SShuffle2(result.m_bx.m_v, m_bz.m_v, 0, 1, 0, 3);

    //Similar as above for other rows
    result.m_by = SShuffle2(m_bx.m_v, m_by.m_v, 1, 3, 1, 3);
    result.m_by = SShuffle(result.m_by.m_v, 0, 2, 3, 3);
    result.m_by = SShuffle2(result.m_by.m_v, m_bz.m_v, 0, 1, 1, 3);

    result.m_bz = SShuffle2(m_bx.m_v, m_by.m_v, 2, 3, 2, 3);
    result.m_bz = SShuffle(result.m_bz.m_v, 0, 2, 3, 3);
    result.m_bz = SShuffle2(result.m_bz.m_v, m_bz.m_v, 0, 1, 2, 3);
    return result;
  }

  void SMatrix3::Transpose(void)
  {
    *this = Transposed();
  }

  SMatrix3 SMatrix3::TransposedMultiply(const SMatrix3& rhs) const
  {
    return Transposed() * rhs;
  }

  SVector3 SMatrix3::TransposedMultiply(const SVector3& rhs) const
  {
    return Transposed() * rhs;
  }

  /*Assume this matrix for comments below:
  [a,b,c]
  [d,e,f]
  [g,h,i]*/
  SMatrix3 SMatrix3::Inverse(void) const
  {
    SMatrix3 result;
    SFloats lhs, rhs;
    SVector3 det;

    //[e,h,b]*[i,c,f]
    lhs = SMultiplyAll(SShuffle(m_by.m_v, 1, 2, 0, 3), SShuffle(m_bz.m_v, 2, 0, 1, 3));
    //[h,b,e]*[f,i,c]
    rhs = SMultiplyAll(SShuffle(m_by.m_v, 2, 0, 1, 3), SShuffle(m_bz.m_v, 1, 2, 0, 3));
    //Subtract each multiplication pair from the corresponding other one
    result.m_bx.m_v = SSubtractAll(lhs, rhs);
    det.m_v = SMultiplyAll(m_bx.m_v, result.m_bx.m_v);

    //[g,a,d]*[f,i,c]
    lhs = SMultiplyAll(SShuffle(m_bx.m_v, 2, 0, 1, 3), SShuffle(m_bz.m_v, 1, 2, 0, 3));
    //[d,g,a]*[i,c,f]
    rhs = SMultiplyAll(SShuffle(m_bx.m_v, 1, 2, 0, 3), SShuffle(m_bz.m_v, 2, 0, 1, 3));
    result.m_by = SSubtractAll(lhs, rhs);

    //[d,g,a]*[h,b,e]
    lhs = SMultiplyAll(SShuffle(m_bx.m_v, 1, 2, 0, 3), SShuffle(m_by.m_v, 2, 0, 1, 3));
    //[g,a,d]*[e,h,b]
    rhs = SMultiplyAll(SShuffle(m_bx.m_v, 2, 0, 1, 3), SShuffle(m_by.m_v, 1, 2, 0, 3));
    result.m_bz = SSubtractAll(lhs, rhs);

    //Easier to do the shuffles here than trying to shuffle with the above math. I imagine it doesn't cost much either
    result.Transpose();

    det.m_v = SAddLower(det.m_v, SShuffle(det.m_v, 1, 1, 2, 3));
    det.m_v = SAddLower(det.m_v, SShuffle(det.m_v, 2, 1, 2, 3));
    det.m_v = SShuffle(det.m_v, 0, 0, 0, 0);
    det = SVector3::Identity.SafeDivide(det);

    return det * result;
  }

  SMatrix3 SMatrix3::Inverse(const SVector3& det) const
  {
    SVector3 invDet = SVector3::Identity.SafeDivide(det);
    SMatrix3 result;
    SFloats lhs, rhs;

    //[e,h,b]*[i,c,f]
    lhs = SMultiplyAll(SShuffle(m_by.m_v, 1, 2, 0, 3), SShuffle(m_bz.m_v, 2, 0, 1, 3));
    //[h,b,e]*[f,i,c]
    rhs = SMultiplyAll(SShuffle(m_by.m_v, 2, 0, 1, 3), SShuffle(m_bz.m_v, 1, 2, 0, 3));
    //Subtract each multiplication pair from the corresponding other one
    result.m_bx.m_v = SSubtractAll(lhs, rhs);

    //[g,a,d]*[f,i,c]
    lhs = SMultiplyAll(SShuffle(m_bx.m_v, 2, 0, 1, 3), SShuffle(m_bz.m_v, 1, 2, 0, 3));
    //[d,g,a]*[i,c,f]
    rhs = SMultiplyAll(SShuffle(m_bx.m_v, 1, 2, 0, 3), SShuffle(m_bz.m_v, 2, 0, 1, 3));
    result.m_by = SSubtractAll(lhs, rhs);

    //[d,g,a]*[h,b,e]
    lhs = SMultiplyAll(SShuffle(m_bx.m_v, 1, 2, 0, 3), SShuffle(m_by.m_v, 2, 0, 1, 3));
    //[g,a,d]*[e,h,b]
    rhs = SMultiplyAll(SShuffle(m_bx.m_v, 2, 0, 1, 3), SShuffle(m_by.m_v, 1, 2, 0, 3));
    result.m_bz = SSubtractAll(lhs, rhs);

    //Easier to do the shuffles here than trying to shuffle with the above math. I imagine it doesn't cost much either
    result.Transpose();
    return invDet * result;
  }

  SVector3 SMatrix3::Determinant(void) const
  {
    //[e,h,b]*[i,c,f]
    SFloats lhs = SMultiplyAll(SShuffle(m_by.m_v, 1, 2, 0, 3), SShuffle(m_bz.m_v, 2, 0, 1, 3));
    //[h,b,e]*[f,i,c]
    SFloats rhs = SMultiplyAll(SShuffle(m_by.m_v, 2, 0, 1, 3), SShuffle(m_bz.m_v, 1, 2, 0, 3));
    //Subtract each multiplication pair from the corresponding other one, and scale by the first column element of the row
    SFloats result = SMultiplyAll(m_bx.m_v, SSubtractAll(lhs, rhs));

    //Add the x,y,z terms and splat the result
    result = SAddLower(result, SShuffle(result, 1, 1, 2, 3));
    result = SAddLower(result, SShuffle(result, 2, 1, 2, 3));
    return SShuffle(result, 0, 0, 0, 0);
  }

  SQuat SMatrix3::ToQuat(void)
  {
    //This has so many branches and single scalar operations I don't think it's worth it to try and SIMD it.
    Matrix3 mat;
    m_bx.Store(mat.m_bx);
    m_by.Store(mat.m_by);
    m_bz.Store(mat.m_bz);
    Quat quat = mat.ToQuat();
    return SQuat(quat.m_v.x, quat.m_v.y, quat.m_v.z, quat.m_w);
  }

  SMatrix3 operator*(const SVector3& lhs, const SMatrix3& rhs)
  {
    return SMatrix3(lhs*rhs.m_bx, lhs*rhs.m_by, lhs*rhs.m_bz);
  }

  SMatrix3 ToSMatrix3(const Matrix3& mat)
  {
    return SMatrix3(ToSVector3(mat.m_bx), ToSVector3(mat.m_by), ToSVector3(mat.m_bz));
  }
}