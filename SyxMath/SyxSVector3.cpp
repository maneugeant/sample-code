#include "Precompile.h"
#include "SyxSVector3.h"
#include "SyxSIMD.h"
#include "SyxVector3.h"

namespace Syx
{
  const static int nOne = -1;
  const static float AllOne = *reinterpret_cast<const float*>(&nOne);

  const SVector3 SVector3::UnitX(1.0f, 0.0f, 0.0f);
  const SVector3 SVector3::UnitY(0.0f, 1.0f, 0.0f);
  const SVector3 SVector3::UnitZ(0.0f, 0.0f, 1.0f);
  const SVector3 SVector3::Zero(0.0f);
  const SVector3 SVector3::Identity(1.0f, 1.0f, 1.0f, 1.0f);
  const SVector3 SVector3::Epsilon(SYX_EPSILON);
  const SVector3 SVector3::BitsX(AllOne, 0.0f, 0.0f, 0.0f);
  const SVector3 SVector3::BitsY(0.0f, AllOne, 0.0f, 0.0f);
  const SVector3 SVector3::BitsZ(0.0f, 0.0f, AllOne, 0.0f);
  const SVector3 SVector3::BitsW(0.0f, 0.0f, 0.0f, AllOne);
  const SVector3 SVector3::BitsAll(AllOne, AllOne, AllOne, AllOne);

  SVector3::SVector3(float x, float y, float z, float w)
  {
    LoadAll(x, y, z, w);
  }

  SVector3::SVector3(float splat)
  {
    SIMDAlign float aligned = splat;
    LoadSplat(&aligned);
  }

  void SVector3::LoadAll(const float* source)
  {
    AssertAlignment(*source);
    m_v = SLoadAll(source);
  }

  void SVector3::LoadAll(float x, float y, float z, float w)
  {
    SIMDAlign const float aligned[4] = { x, y, z, w };
    LoadAll(reinterpret_cast<const float*>(aligned));
  }

  void SVector3::LoadAll(const Vector3& in)
  {
    LoadAll(in.x, in.y, in.z);
  }

  void SVector3::LoadSplat(const float* source)
  {
    AssertAlignment(*source);
    m_v = SLoadSplat(source);
  }

  void SVector3::Store(float* destination) const
  {
    AssertAlignment(*destination);
    SStoreAll(destination, m_v);
  }

  void SVector3::Store(Vector3& destination) const
  {
    SIMDAlign float aligned[4];
    SStoreAll(aligned, m_v);
    destination.x = aligned[0];
    destination.y = aligned[1];
    destination.z = aligned[2];
  }

  SVector3 SVector3::operator+(const SVector3& rhs) const
  {
    return SVector3(SAddAll(m_v, rhs.m_v));
  }

  SVector3 SVector3::operator-(const SVector3& rhs) const
  {
    return SVector3(SSubtractAll(m_v, rhs.m_v));
  }

  SVector3 SVector3::operator-(void) const
  {
    return SVector3::Zero - *this;
  }

  SVector3 SVector3::operator*(const SVector3& rhs) const
  {
    return SVector3(SMultiplyAll(m_v, rhs.m_v));
  }

  SVector3 SVector3::operator/(const SVector3& rhs) const
  {
    return SVector3(SDivideAll(m_v, rhs.m_v));
  }

  SVector3 SVector3::operator&(const SVector3& rhs) const
  {
    return SAnd(m_v, rhs.m_v);
  }

  SVector3 SVector3::operator|(const SVector3& rhs) const
  {
    return SOr(m_v, rhs.m_v);
  }

  SVector3& SVector3::operator+=(const SVector3& rhs)
  {
    m_v = SAddAll(m_v, rhs.m_v);
    return *this;
  }

  SVector3& SVector3::operator-=(const SVector3& rhs)
  {
    m_v = SSubtractAll(m_v, rhs.m_v);
    return *this;
  }

  SVector3& SVector3::operator*=(const SVector3& rhs)
  {
    m_v = SMultiplyAll(m_v, rhs.m_v);
    return *this;
  }

  SVector3& SVector3::operator/=(const SVector3& rhs)
  {
    m_v = SDivideAll(m_v, rhs.m_v);
    return *this;
  }

  SVector3 SVector3::operator==(const SVector3& rhs) const
  {
    return Equal(rhs, SVector3::Epsilon);
  }

  SVector3 SVector3::operator!=(const SVector3& rhs) const
  {
    return NotEqual(rhs, SVector3::Epsilon);
  }

  SVector3 SVector3::Equal(const SVector3& rhs, const SVector3& epsilon) const
  {
    SFloats diff = SAbsAll(SSubtractAll(m_v, rhs.m_v));
   //This will make each component reflect equality with the corresponding one
    SFloats cmp = SLessEqualAll(diff, epsilon.m_v);
   //And everything so the result is only true if all components match
    //xyz&yxx
    cmp = SAnd(cmp, SShuffle(cmp, 1, 0, 0, 3));
    //xyz&yxx&zzy
    return SVector3(SAnd(cmp, SShuffle(cmp, 2, 2, 1, 3)));
  }

  SVector3 SVector3::NotEqual(const SVector3& rhs, const SVector3& epsilon) const
  {
    SFloats diff = SAbsAll(SSubtractAll(m_v, rhs.m_v));
   //This will make each component reflect inequality with the corresponding one
    SFloats cmp = SGreaterAll(diff, epsilon.m_v);
   //Or everything so the result is true if any components don't match
    //xyz&yxx
    cmp = SOr(cmp, SShuffle(cmp, 1, 0, 0, 3));
    //xyz&yxx&zzy
    return SVector3(SOr(cmp, SShuffle(cmp, 2, 2, 1, 3)));
  }

  //From Erin Catto's box2d forum
  void SVector3::GetBasis(SVector3& resultA, SVector3& resultB) const
  {
    // Suppose vector a has all equal components and is a unit vector: a = (s, s, s)
    // Then 3*s*s = 1, s = sqrt(1/3) = 0.57735. This means that at least one component of a
    // unit vector must be greater or equal to 0.57735.
    SIMDAlign float store[4];
    Store(store);
    if(abs(store[0]) >= 0.57735f)
      resultA = SVector3(store[1], -store[0], 0.0f).Normalized();
    else
      resultA = SVector3(0.0f, store[2], -store[1]).Normalized();

    resultB = Cross(resultA);
  }

  SVector3 SVector3::Length(void) const
  {
    SFloats dot = Dot(*this).m_v;
    //Put sqrt in lower value
    dot = SSqrtLower(dot);
    //Splat it all over
    return SVector3(SShuffle(dot, 0, 0, 0, 0));
  }

  SVector3 SVector3::Length2(void) const
  {
    return Dot(*this);
  }

  SVector3 SVector3::Distance(const SVector3& other) const
  {
    return (*this - other).Length();
  }

  SVector3 SVector3::Distance2(const SVector3& other) const
  {
    return (*this - other).Length2();
  }

  SVector3 SVector3::Dot(const SVector3& other) const
  {
    SFloats mul = SMultiplyAll(m_v, other.m_v);
    //yxy
    SFloats temp = SShuffle(mul, 1, 0, 1, 3);
    SFloats result = SAddAll(mul, temp);
    //zzx
    temp = SShuffle(mul, 2, 2, 0, 3);
    result = SAddAll(result, temp);
    //Need to put the value in the w element otherwise we get issues with quaternions
    return SShuffle(result, 0, 1, 2, 2);
  }

  SVector3 SVector3::Dot4(const SVector3& other) const
  {
    SFloats mul = SMultiplyAll(m_v, other.m_v);
    //yxyz
    SFloats temp = SShuffle(mul, 1, 0, 1, 2);
    SFloats result = SAddAll(mul, temp);
    //zzxy
    temp = SShuffle(mul, 2, 2, 0, 1);
    result = SAddAll(result, temp);
    //wwwx
    temp = SShuffle(mul, 3, 3, 3, 0);
    return SAddAll(result, temp);
  }

  SVector3 SVector3::Cross(const SVector3& other) const
  {
    //[y,z,x]*[z,x,y]
    SFloats lMul = SMultiplyAll(SShuffle(m_v, 1, 2, 0, 3), SShuffle(other.m_v, 2, 0, 1, 3));
    //[z,x,y]*[y,z,x]
    SFloats rMul = SMultiplyAll(SShuffle(m_v, 2, 0, 1, 3), SShuffle(other.m_v, 1, 2, 0, 3));
    //[y,z,x]*[z,x,y] - [z,x,y]*[y,z,x]
    return SVector3(SSubtractAll(lMul, rMul));
  }

  //These will have multiple 1 fields if values are the same
  SVector3 SVector3::LeastSignificantAxis(void) const
  {
    //Compare all agains all others, anding every time, so you end up with all bits set in the right value
    //[x,y,z]<[y,x,x]
    SFloats result = SLessEqualAll(m_v, SShuffle(m_v, 1, 0, 0, 3));
    //[x,y,z]<z,z,y]
    result = SAnd(result, SLessEqualAll(m_v, SShuffle(m_v, 2, 2, 1, 3)));
    return result;
  }

  SVector3 SVector3::MostSignificantAxis(void) const
  {
    //Compare all agains all others, anding every time, so you end up with all bits set in the right value
    //[x,y,z]<[y,x,x]
    SFloats result = SGreaterEqualAll(m_v, SShuffle(m_v, 1, 0, 0, 3));
    //[x,y,z]<z,z,y]
    result = SAnd(result, SGreaterEqualAll(m_v, SShuffle(m_v, 2, 2, 1, 3)));
    return result;
  }

  SVector3 SVector3::Normalized(void) const
  {
    return *this/Length();
  }

  SVector3 SVector3::SafeNormalized(void) const
  {
    return SafeDivide(Length());
  }

  SVector3 SVector3::SafeDivide(const SVector3& rhs) const
  {
    SFloats mask = SNotEqualAll(rhs.m_v, Zero.m_v);
    //If it was division by zero the result will mask will be 0, so the result will be cleared
    return SVector3(SAnd(mask, SDivideAll(m_v, rhs.m_v)));
  }

  SVector3 SVector3::Abs(void) const
  {
    return SVector3(SAbsAll(m_v));
  }

  SVector3 SVector3::ProjVec(const SVector3& onto) const
  {
    return onto*(Dot(onto)/onto.Dot(onto));
  }

  SVector3 SVector3::ProjVecScalar(const SVector3& onto) const
  {
    return Dot(onto)/onto.Dot(onto);
  }

  SVector3 SVector3::SafeDivide(const SVector3& vec, const SVector3& div)
  {
    return vec.SafeDivide(div);
  }

  SVector3 SVector3::Lerp(const SVector3& start, const SVector3& end, const SVector3& t)
  {
    return start + t*(end - start);
  }

  SVector3 SVector3::Abs(const SVector3& in)
  {
    return in.Abs();
  }

  SVector3 SVector3::PointLineDistanceSQ(const SVector3& point, const SVector3& start, const SVector3& end)
  {
    SVector3 projPoint = start + (point - start).ProjVec(end - start);
    return (point - projPoint).Length2();
  }

  SVector3 SVector3::CCWTriangleNormal(const SVector3& a, const SVector3& b, const SVector3& c)
  {
    return (b - a).Cross(c - a);
  }

  SVector3 SVector3::PerpendicularLineToPoint(const SVector3& line, const SVector3& lineToPoint)
  {
    return line.Cross(lineToPoint).Cross(line);
  }

  SVector3 SVector3::BarycentricToPoint(const SVector3& a, const SVector3& b, const SVector3& c, const SVector3& ba, const SVector3& bb, const SVector3& bc)
  {
    return a*ba + b*bb + c*bc;
  }

  SVector3 SVector3::ProjVec(const SVector3& vec, const SVector3& onto)
  {
    return vec.ProjVec(onto);
  }

  SVector3 SVector3::ProjVecScalar(const SVector3& vec, const SVector3& onto)
  {
    return vec.ProjVecScalar(onto);
  }

  SVector3 SVector3::PointPlaneProj(const SVector3& point, const SVector3& normal, const SVector3& onPlane)
  {
    return point - ProjVec(point - onPlane, normal);
  }

  SVector3 SVector3::GetScalarT(const SVector3& start, const SVector3& end, const SVector3& pointOnLine)
  {
    SVector3 startToEnd = end - start;
    //Slow, should optimize out of this is used often
    SIMDAlign float store[4];
    startToEnd.Store(store);

    unsigned nonZeroAxis = 0;
    for(unsigned i = 0; i < 3; ++i)
      if(abs(startToEnd[i]) > SYX_EPSILON)
      {
        nonZeroAxis = i;
        break;
      }

    return SVector3((pointOnLine[nonZeroAxis] - start[nonZeroAxis]) / startToEnd[nonZeroAxis]);
  }

  SVector3 SVector3::Length(const SVector3& in)
  {
    return in.Length();
  }

  SVector3 SVector3::Length2(const SVector3& in)
  {
    return in.Length2();
  }

  SVector3 SVector3::Distance(const SVector3& lhs, const SVector3& rhs)
  {
    return lhs.Distance(rhs);
  }

  SVector3 SVector3::Distance2(const SVector3& lhs, const SVector3& rhs)
  {
    return lhs.Distance2(rhs);
  }

  SVector3 SVector3::Dot(const SVector3& lhs, const SVector3& rhs)
  {
    return lhs.Dot(rhs);
  }

  SVector3 Dot4(const SVector3& lhs, const SVector3& rhs)
  {
    return lhs.Dot4(rhs);
  }

  SVector3 SVector3::Cross(const SVector3& lhs, const SVector3& rhs)
  {
    return lhs.Cross(rhs);
  }

  SVector3 SVector3::LeastSignificantAxis(const SVector3& in)
  {
    return in.LeastSignificantAxis();
  }
  
  SVector3 SVector3::MostSignificantAxis(const SVector3& in)
  {
    return in.MostSignificantAxis();
  }

  SVector3 SVector3::Reciprocal(void) const
  {
    return SafeDivide(Identity, *this);
  }

  SVector3 ToSVector3(const Vector3& vec)
  {
    return SVector3(vec.x, vec.y, vec.z);
  }

  Vector3 ToVector3(const SVector3& vec)
  {
    Vector3 result;
    vec.Store(result);
    return result;
  }
}