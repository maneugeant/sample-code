#pragma once
#include "SyxSVector3.h"

namespace Syx
{
  struct SVector3;
  struct SMatrix3;

  struct SQuat
  {
    SQuat(const SVector3& ijk, float w);
    SQuat(const SVector3& ijkw);
    SQuat(SFloats v): m_v(v) {}
    SQuat(float i, float j, float k, float w);
    SQuat(void) {};

    SQuat operator*(const SQuat& rhs) const;
    SQuat& operator*=(const SQuat& rhs);
    SQuat operator*(const SVector3& rhs) const;
    SQuat& operator*=(const SVector3& rhs);
    SQuat operator+(const SQuat& rhs) const;
    SQuat& operator+=(const SQuat& rhs);
    SQuat operator/(const SVector3& rhs) const;
    SQuat& operator/=(const SVector3& rhs);
    SQuat operator-(void) const;

    SVector3 Rotate(const SVector3& vec) const;

    SVector3 Length(void) const;
    SVector3 Length2(void) const;

    SQuat Normalized(void) const;
    void Normalize(void);

    SQuat Inversed(void) const;
    void Inverse(void);

    SMatrix3 ToMatrix(void) const;

    static SQuat AxisAngle(const SVector3& axis, float angle);

    static const SQuat Zero;

    SVector3 m_v;
  };

  SQuat operator*(const SVector3& lhs, const SQuat& rhs);
}