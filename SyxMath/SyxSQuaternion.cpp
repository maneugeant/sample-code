#include "Precompile.h"
#include "SyxSQuaternion.h"
#include "SyxSVector3.h"
#include "SyxSMatrix3.h"
#include "SyxSIMD.h"

namespace Syx
{
  const SQuat SQuat::Zero(0.0f, 0.0f, 0.0f, 0.0f);

  SQuat::SQuat(const SVector3& ijk, float w)
  {
    m_v = ijk;
    m_v.m_v.m128_f32[3] = w;
  }

  SQuat::SQuat(const SVector3& ijkw): m_v(ijkw) {}

  SQuat::SQuat(float i, float j, float k, float w): m_v(i, j, k, w) {}

  /*L=(x,y,z,w), R=(q,r,s,t)
  L and R is just the first 3 elements, not w and t
  w*R + t*L + LcrossR, w*t - LdotR
   [wq+xt+ys-zr]
   [wr+yt+zq-xs]
   [ws+zt+xr-yq]
  -[xq+yr+zs-wt]

   //Last row rearranged to from equation for less operations
   [wt-xq-yr-zs] = wt-(qx+yr+zs) = -(qx+yr+zs)+wt = -(qx+yr+zs-wt)*/
  SQuat SQuat::operator*(const SQuat& rhs) const
  {
    SFloats result = SMultiplyAll(SShuffle(m_v.m_v, 3, 3, 3, 0), SShuffle(rhs.m_v.m_v, 0, 1, 2, 0));
    result = SAddAll(result, SMultiplyAll(SShuffle(m_v.m_v, 0, 1, 2, 1), SShuffle(rhs.m_v.m_v, 3, 3, 3, 1)));
    result = SAddAll(result, SMultiplyAll(SShuffle(m_v.m_v, 1, 2, 0, 2), SShuffle(rhs.m_v.m_v, 2, 0, 1, 2)));
    result = SSubtractAll(result, SMultiplyAll(SShuffle(m_v.m_v, 2, 0, 1, 3), SShuffle(rhs.m_v.m_v, 1, 2, 0, 3)));

    static const SVector3 flipWSign(1.0f, 1.0f, 1.0f, -1.0f);
    result = SMultiplyAll(result, flipWSign.m_v);
    return SQuat(result);
  }

  SQuat& SQuat::operator*=(const SQuat& rhs)
  {
    return *this = *this * rhs;
  }

  SQuat SQuat::operator*(const SVector3& rhs) const
  {
    return SQuat(m_v*rhs);
  }

  SQuat& SQuat::operator*=(const SVector3& rhs)
  {
    return *this = *this * rhs;
  }

  SQuat SQuat::operator+(const SQuat& rhs) const
  {
    return m_v + rhs.m_v;
  }

  SQuat& SQuat::operator+=(const SQuat& rhs)
  {
    return *this = *this + rhs;
  }

  SQuat  SQuat::operator/(const SVector3& rhs) const
  {
    return m_v / rhs.m_v;
  }

  SQuat& SQuat::operator/=(const SVector3& rhs)
  {
    return *this = *this / rhs;
  }

  SQuat SQuat::operator-(void) const
  {
    static const SVector3 negation(-1.0f, -1.0f, -1.0f, 1.0f);
    return *this * negation;
  }

  //Quaternion multiplication assuming rhs has 0 as its w term
  SFloats QuatMultNoW(SFloats lhs, SFloats rhs)
  {
    //Same as quaternion multiplication but with t terms removed since it's 0
    SFloats result = SMultiplyAll(SShuffle(lhs, 3, 3, 3, 0), SShuffle(rhs, 0, 1, 2, 0));
    result = SAddAll(result, SMultiplyAll(SShuffle(lhs, 1, 2, 0, 1), SShuffle(rhs, 2, 0, 1, 1)));
    static const SVector3 flipWSign(1.0f, 1.0f, 1.0f, -1.0f);
    result = SSubtractAll(result, SMultiplyAll(flipWSign.m_v, SMultiplyAll(SShuffle(lhs, 2, 0, 1, 2), SShuffle(rhs, 1, 2, 0, 2))));

    result = SMultiplyAll(result, flipWSign.m_v);
    return result;
  }

  SFloats QuatMultZeroW(SFloats lhs, SFloats rhs)
  {
    SFloats result = SMultiplyAll(SShuffle(lhs, 3, 3, 3, 0), SShuffle(rhs, 0, 1, 2, 0));
    result = SAddAll(result, SMultiplyAll(SShuffle(lhs, 0, 1, 2, 1), SShuffle(rhs, 3, 3, 3, 1)));
    result = SAddAll(result, SMultiplyAll(SShuffle(lhs, 1, 2, 0, 2), SShuffle(rhs, 2, 0, 1, 2)));
    result = SSubtractAll(result, SMultiplyAll(SShuffle(lhs, 2, 0, 1, 3), SShuffle(rhs, 1, 2, 0, 3)));

    static const SFloats zeroW = SOr(SOr(SVector3::BitsX.m_v, SVector3::BitsY.m_v), SVector3::BitsZ.m_v);
    result = SAnd(result, zeroW);
    return result;
  }

  SVector3 SQuat::Rotate(const SVector3& vec) const
  {
    return QuatMultZeroW(QuatMultNoW(m_v.m_v, vec.m_v), (-(*this)).m_v.m_v);
  }

  SVector3 SQuat::Length(void) const
  {
    return SSqrtAll(Length2().m_v);
  }

  SVector3 SQuat::Length2(void) const
  {
    return m_v.Dot4(m_v);
  }

  SQuat SQuat::Normalized(void) const
  {
    return *this / Length();
  }

  void SQuat::Normalize(void)
  {
    *this /= Length();
  }

  SQuat SQuat::Inversed(void) const
  {
    return -*this;
  }

  void SQuat::Inverse(void)
  {
    *this = -*this;
  }

  //-2yy-2zz+1, 2xy-2zw  , 2xz+2yw
  // 2xy+2zw  ,-2xx-2zz+1, 2yz-2xw
  // 2xz-2yw  , 2yz+2xw  ,-2xx-2yy+1
  SMatrix3 SQuat::ToMatrix(void) const
  {
    static const SVector3 allTwo(-2.0f, 2.0f, 2.0f, 2.0f);
    SMatrix3 result;

    //Same formula as on standard quaternion, but done per column from left to right as above
    //-2yy-2zz+1
    // 2xy+2zw  
    // 2xz-2yw  
    SFloats cl = allTwo.m_v;
    SFloats rhs = SShuffle(m_v.m_v, 1, 0, 0, 3);
    cl = SMultiplyAll(cl, rhs);
    rhs = SShuffle(m_v.m_v, 1, 1, 2, 3);
    cl = SMultiplyAll(cl, rhs);

    SFloats cr = SShuffle(allTwo.m_v, 0, 1, 0, 3);
    rhs = SShuffle(m_v.m_v, 2, 2, 1, 3);
    cr = SMultiplyAll(cr, rhs);
    rhs = SShuffle(m_v.m_v, 2, 3, 3, 3);
    cr = SMultiplyAll(cr, rhs);
    cl = SAddAll(cl, cr);
    result.m_bx = SAddAll(cl, SVector3::UnitX.m_v);

    // 2xy-2zw  
    //-2xx-2zz+1
    // 2yz+2xw  
    cl = SShuffle(allTwo.m_v, 1, 0, 1, 3);
    rhs = SShuffle(m_v.m_v, 0, 0, 1, 3);
    cl = SMultiplyAll(cl, rhs);
    rhs = SShuffle(m_v.m_v, 1, 0, 2, 3);
    cl = SMultiplyAll(cl, rhs);

    cr = SShuffle(allTwo.m_v, 0, 0, 1, 3);
    rhs = SShuffle(m_v.m_v, 2, 2, 0, 3);
    cr = SMultiplyAll(cr, rhs);
    rhs = SShuffle(m_v.m_v, 3, 2, 3, 3);
    cr = SMultiplyAll(cr, rhs);
    cl = SAddAll(cl, cr);
    result.m_by = SAddAll(cl, SVector3::UnitY.m_v);

    // 2xz+2yw
    // 2yz-2xw
    //-2xx-2yy+1
    cl = SShuffle(allTwo.m_v, 1, 1, 0, 3);
    rhs = SShuffle(m_v.m_v, 0, 1, 0, 3);
    cl = SMultiplyAll(cl, rhs);
    rhs = SShuffle(m_v.m_v, 2, 2, 0, 3);
    cl = SMultiplyAll(cl, rhs);

    cr = SShuffle(allTwo.m_v, 1, 0, 0, 3);
    rhs = SShuffle(m_v.m_v, 1, 0, 1, 3);
    cr = SMultiplyAll(cr, rhs);
    rhs = SShuffle(m_v.m_v, 3, 3, 1, 3);
    cr = SMultiplyAll(cr, rhs);
    cl = SAddAll(cl, cr);
    result.m_bz = SAddAll(cl, SVector3::UnitZ.m_v);
    return result;
  }

  SQuat SQuat::AxisAngle(const SVector3& axis, float angle)
  {
    angle*= 0.5f;
    SVector3 sinAngle(sin(angle));
    return SQuat(axis*sinAngle, cos(angle));
  }

  SQuat operator*(const SVector3& lhs, const SQuat& rhs)
  {
    return rhs * lhs;
  }
}