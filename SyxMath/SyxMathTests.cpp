//Test SIMD library against scalar library I know works
#include "Precompile.h"
#include "SyxMathTests.h"
#include "SyxMath.h"
#include "SyxTestHelpers.h"

namespace Syx
{
  bool TEST_FAILED = false;

  bool TestAllMath(void)
{
    TEST_FAILED = false;
    TestSVector3();
    if(TEST_FAILED) return TEST_FAILED;
    TestSMatrix3();
    if(TEST_FAILED) return TEST_FAILED;
    TestSQuat();
    return TEST_FAILED;
  }

  bool TestSVector3(void)
{
    TEST_FAILED = false;
    Vector3 va(1.0f, 2.0f, 3.0f);
    Vector3 vb(2.0f, -2.0f, 1.0f);
    SVector3 sa(1.0f, 2.0f, 3.0f);
    SVector3 sb(2.0f, -2.0f, 1.0f);
    SVector3 s4(1.0f, 2.0f, 3.0f, 4.0f);

    CheckResult(sa, va);
    CheckResult(SVector3(1.0f), Vector3(1.0f));
    CheckResult(sa + sb, va + vb);
    CheckResult(sa - sb, va - vb);
    CheckResult(-sa, -va);
    CheckResult(sa*SVector3(2.0f), va*2.0f);
    CheckResult(sa/SVector3(2.0f), va/2.0f);
    CheckResult((SVector3(2.0f) == SVector3(3.0f))&SVector3::Identity, Vector3(0.0f));
    CheckResult((SVector3(2.0f) == SVector3(2.0f))&SVector3::Identity, Vector3(1.0f));
    CheckResult((SVector3(2.0f) != SVector3(3.0f))&SVector3::Identity, Vector3(1.0f));
    CheckResult((SVector3(2.0f) != SVector3(2.0f))&SVector3::Identity, Vector3(0.0f));
    CheckResult((sa == SVector3(1.0f, 2.0f, 4.0f))&SVector3::Identity, Vector3(0.0f));
    CheckResult(sa.Length(), va.Length());
    CheckResult(sa.Length2(), va.Length2());
    CheckResult(sa.Distance(sb), va.Distance(vb));
    CheckResult(sa.Distance2(sb), va.Distance2(vb));
    CheckResult(sa.Dot(sb), va.Dot(vb));
    CheckResult(s4.Dot4(s4), Vector3(30.0f));
    CheckResult(sa.Cross(sb), va.Cross(vb));
    CheckResult(sa.LeastSignificantAxis()&SVector3::Identity, Vector3(1.0f, 0.0f, 0.0f));
    CheckResult(sa.MostSignificantAxis()&SVector3::Identity, Vector3(0.0f, 0.0f, 1.0f));
    CheckResult(sa.Normalized(), va.Normalized());
    CheckResult(SVector3::Zero.SafeNormalized(), Vector3::Zero.SafeNormalized());
    CheckResult(sa*sb, Vector3::Scale(va, vb));
    return TEST_FAILED;
  }

  bool TestSMatrix3(void)
  {
    TEST_FAILED = false;
    float m[9] = { 1.0f, 2.0f, 3.0f,
                  -1.0f, 3.0f, 5.0f,
                   0.5f, -2.0f, 6.0f };
    float r[9] = { 0.612361f, 0.729383f, 0.198896f,
                   0.779353f, 0.334213f, 0.569723f,
                   0.176786f, 0.126579f, 0.15549f };
    float x = 2.0f;
    float y = 5.5f;
    float z = 0.7f;

    Vector3 v(x, y, z);
    SVector3 sv(x, y, z);

    Matrix3 ma(m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8]);
    SMatrix3 sa(m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8]);
    Matrix3 mb(r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8]);
    SMatrix3 sb(r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8]);

    CheckResult(sa, ma);
    CheckResult(sa.Transposed(), ma.Transposed());
    CheckResult(sa*sb, ma*mb);
    CheckResult(sa+sb, ma+mb);
    CheckResult(sa-sb, ma-mb);
    CheckResult(sa.TransposedMultiply(sb), ma.TransposedMultiply(mb));
    CheckResult(sa*sv, ma*v);
    CheckResult(sa.TransposedMultiply(sv), ma.TransposedMultiply(v));
    CheckResult(sb.Determinant(), mb.Determinant());
    CheckResult(sb.Inverse(sb.Determinant()), mb.Inverse(mb.Determinant()));
    CheckResult(sb.Inverse(), mb.Inverse());
    CheckResult(sa.ToQuat(), ma.ToQuat());
    return TEST_FAILED;
  }

  bool TestSQuat(void)
  {
    TEST_FAILED = false;
    Vector3 axisA(1.0f, 2.0f, 0.5f);
    axisA.Normalize();
    float angleA = 1.0f;
    SQuat sqA = SQuat::AxisAngle(SVector3(axisA.x, axisA.y, axisA.z), angleA);
    Quat qA = Quat::AxisAngle(axisA, angleA);

    Vector3 axisB(0.5f, 2.5f, 1.5f);
    axisB.Normalize();
    float angleB = 0.343f;
    SQuat sqB = SQuat::AxisAngle(SVector3(axisB.x, axisB.y, axisB.z), angleB);
    Quat qB = Quat::AxisAngle(axisB, angleB);

    CheckResult(sqA, qA);
    CheckResult(-sqA, -qA);
    CheckResult(sqA + sqB, qA + qB);
    CheckResult(sqA/SVector3(angleB), qA/angleB);
    CheckResult(sqA*SVector3(angleB), qA*angleB);
    CheckResult(sqA.Length2(), Vector3(qA.Length2()));
    CheckResult(sqA.Length(), Vector3(qA.Length()));
    CheckResult(sqA.Normalized(), qA.Normalized());
    CheckResult(sqA*sqB, qA*qB);
    CheckResult(sqA.Rotate(ToSVector3(axisB)), qA*axisB);
    CheckResult(sqA.ToMatrix(), qA.ToMatrix());
    return TEST_FAILED;
  }

  bool TestVector2(void)
  {
    Vector2 x = Vector2::s_unitX;
    Vector2 y = Vector2::s_unitY;
    Vector2 i = Vector2::s_identity;
    Vector2 z = Vector2::s_zero;
    Vector2 a(1.4f, -8.1f);
    Vector2 b(-4.6f, 13.9f);

    CheckResult((x == x));
    CheckResult(!(x == y));
    CheckResult(x != y);
    CheckResult(x + y, i);
    CheckResult(i - x - y, z);
    CheckResult(3.0f*(x + 2.0f*y), Vector2(3.0f, 6.0f));
    CheckResult(abs(x.Dot(y)) < SYX_EPSILON);
    CheckResult(abs(x.Cross(y) - 1.0f) < SYX_EPSILON);
    CheckResult(abs(y.Cross(x) + 1.0f) < SYX_EPSILON);
    CheckResult(a.Lerp(b, 0.0f) == a);
    CheckResult(a.Lerp(b, 1.0f) == b);
    CheckResult(a.Lerp(b, 0.5f) == (a + b)/2.0f);
    CheckResult(x.Slerp(y, 1.0f) == y);
    CheckResult(x.Slerp(y, 0.0f) == x);
    CheckResult(x.Slerp(y, 0.5f) == Vector2(1.0f, 1.0f).Normalized());
    //Ambiguous case, but producing an orthogonal vector instead of zero is what we want
    CheckResult(abs((x.Slerp(-x, 0.5f)).Cross(x)) - 1.0f < SYX_EPSILON);
    CheckResult(x.Slerp(x, 0.5f) == x);
    //Use bigger epsilon because all the trig and normalization adds up to more error than SYX_EPSILON
    CheckResult(Vector2(-1, 1).Normalized().Slerp(Vector2(-1, -1).Normalized(), 0.5f).Equal(-x, 0.01f));
    CheckResult(Vector2(2.0f, 7.0f).Proj(3.0f*x) == 2.0f*x);
    CheckResult(y.Rotate(SYX_PI_4) == Vector2(-1.0f, 1.0f).Normalized());
    return TEST_FAILED;
  }

  bool TestMatrix2(void)
  {
    Matrix2 rot45 = Matrix2::Rotate(SYX_PI_4);
    Vector2 x = Vector2::s_unitX;
    Vector2 y = Vector2::s_unitY;

    CheckResult(Matrix2::s_identity + Matrix2::s_identity == 2.0f*Matrix2::s_identity);
    CheckResult(rot45*rot45 == Matrix2::Rotate(SYX_PI_2));
    CheckResult(rot45.TransposedMultiply(rot45) == Matrix2::s_identity);
    CheckResult(rot45*rot45.Transposed() == Matrix2::s_identity);
    CheckResult(Matrix2::RotationFromUp(Vector2::s_unitY) == Matrix2::s_identity);
    CheckResult(Matrix2::RotationFromRight(Vector2::s_unitX) == Matrix2::s_identity);
    CheckResult(rot45*x == Vector2(1.0f, 1.0f).Normalized());
    CheckResult(rot45.TransposedMultiply(x) == Vector2(1.0f, -1.0f).Normalized());
    CheckResult(Matrix2::Rotate(x, y)*x == y);
    return TEST_FAILED;
  }
}