#pragma once

namespace Syx
{
  bool TestAllMath(void);
  bool TestSVector3(void);
  bool TestSMatrix3(void);
  bool TestSQuat(void);
  bool TestVector2(void);
  bool TestMatrix2(void);
}